from prody import *
#import matplotlib.pyplot as plt
import numpy as np
import math
import multiprocessing, logging
import datetime
import mkdir_p
import os
import dcdTools
#import pqrTools
import glob
import shutil

def analyseTraj(traj_array,stride=False,multipleCores=False,toFile=False,mode='RMSD',customSelections=''):
	
	#mpl = multiprocessing.log_to_stderr()
	#mpl.setLevel(multiprocessing.SUBDEBUG)

	numTraj = len(traj_array)
	dataTraj = ['']*(numTraj)

	for i in range(numTraj):

		traj_tuple = traj_array[i]

		activeTraj = Trajectory(traj_tuple[1])
		activeNumFrames = activeTraj.numFrames()
		indexArray = np.arange(0, activeNumFrames + 1)

		if multipleCores == True:

			# We will split the calculation into two parts: for rmsd-versus-time and rmsf (mean residue rmsd). 
			# The reason is, while for rmsd-versus-time, splitting calculation into frames is the most efficient, for rmsf, 
			# it is splitting into residue numbers.

			# First, do the calculation for rmsd-time profiles.
			availableCores = multiprocessing.cpu_count()
			pool = multiprocessing.Pool()
			listIndexArrays = np.array_split(indexArray,availableCores)
			global dataSingleTrajChunks
			dataSingleTrajChunks = list()
			
			# Define a callback function to merge chunks obtained from independent processes.
			# def mergeChunks(dataSingleTraj):
				
			# 	global dataSingleTrajChunks
			# 	dataSingleTrajChunks.append(dataSingleTraj)
			# 	return dataSingleTrajChunks

			for indexArrayChunk in listIndexArrays:

				dataSingleTrajChunks.append(pool.apply(analyseTrajSingleCore, args = (traj_tuple,indexArrayChunk,mode,customSelections)))
		
			pool.close()
			pool.join()

			dataTraj[i] = dataSingleTrajChunks

		elif multipleCores == False:

			dataTraj = analyseTrajSingleCore(traj_tuple,indexArray,mode,customSelections)

		if toFile:

			write2file(dataTraj,traj_tuple)

	return dataTraj

def analyseTrajSingleCore(traj_tuple,indexArray,mode,customSelections):
	
	activeSystemString = traj_tuple[0]
	activeTrajString = traj_tuple[1]

	activeSystem = parsePDB(activeSystemString)
	activeTraj = Trajectory(activeTrajString)
	activeTraj.link(activeSystem)

	#print ('indexArray for this chunk is: %i' % indexArray)
	lenActiveChunk = len(indexArray)

	# Start a dictionary to save all data.
	dataSingleTraj = {'full':'','chains':'','custom':'','residues':''}
	print ('Starting calculation for trajectory %s' % activeTrajString)
		
	# Set the reference coordinates to FRAME ZERO. THIS IS IMPORTANT!!!!!
	activeTraj.setCoords(activeTraj.getFrame(0))
		
	# Get the number of chains and residues.
	numChains = activeSystem.numChains()
	numResidues = activeSystem.numResidues()

	if mode == 'RMSD': # Calculate RMSD-time profile of full structure and individual chains separately.

		dataFull,dataChains,dataCustom = getRMSD(activeSystem,activeTraj,indexArray,customSelections,lenActiveChunk,numChains,numResidues)
		dataSingleTraj['chains'] = dataChains
		dataSingleTraj['full'] = dataFull
		dataSingleTraj['custom'] = dataCustom

	elif mode == 'RMSD-residue': # Calculate RMSD-time profile of each residue.

		dataResidues = getResidueData(activeSystem,activeTraj,indexArray,mode='RMSD')
		dataSingleTraj['residues'] = dataResidues

	elif mode =='RMSF': # Calculate the RMSF value of each residue.

		dataResidues = getResidueData(activeSystem,activeTraj,indexArray,mode='RMSF')
		dataSingleTraj['residues'] = dataResidues

	elif mode == 'Residues': # Calculate both RMSD-time profile and RMSF value of each residue.

		dataResidues = getResidueData(activeSystem,activeTraj,indexArray,mode='RMSD')
		dataResidues = getResidueData(activeSystem,activeTraj,indexArray,mode='RMSF')
		dataSingleTraj['residues'] = dataResidues

	elif mode == 'all': # Calculate everything.
				
		dataFull,dataChains,dataCustom = getRMSD(activeSystem,activeTraj,indexArray,customSelections,lenActiveChunk,numChains,numResidues)
		dataSingleTraj['chains'] = dataChains
		dataSingleTraj['full'] = dataFull
		dataSingleTraj['custom'] = dataCustom

		dataResidues = getResidueData(activeSystem,activeTraj,indexArray,mode='all')
		dataSingleTraj['residues'] = dataResidues

	return dataSingleTraj	
	
def getRMSD(activeSystem,activeTraj,indexArray,customSelections,lenActiveChunk,numChains,numResidues):

	## Calculate RMSD of full system.
	dataFull = {'all': np.zeros(lenActiveChunk),'ca': np.zeros(lenActiveChunk)}

	print 'Calculating RMSD of full system'
	activeTraj.setAtoms(activeSystem.select('all'))
	dataFull['all'] = calculateRMSD(activeSystem,activeTraj,indexArray)
	
	## Calculate RMSD of CA atoms only.
	print 'Calculating RMSD of CA atoms only'
	activeTraj.setAtoms(activeSystem.select('name CA'))
	dataFull['ca'] = calculateRMSD(activeSystem,activeTraj,indexArray)

	# Calculate RMSD of custom selections, if they are provided.
	dataCustom =  dict()

	if customSelections:

		for selection in customSelections:
			
			activeTraj.setAtoms(activeSystem.select(selection))
			dataCustom[selection] = calculateRMSD(activeSystem,activeTraj,indexArray)

	## Calculate for each chain.
	dataChains = ['']*numChains
	
	for j in range(numChains):
		
		dataSingleChain = {'all': np.zeros(lenActiveChunk),'ca': np.zeros(lenActiveChunk)}
		print ('Calculating RMSD of chain %i (all atoms)' % j)
		activeTraj.setAtoms(activeSystem.select(('chindex %i' % j)))
		dataSingleChain['all'] = calculateRMSD(activeSystem,activeTraj,indexArray)
		
		print ('Calculating RMSD of chain %i (CA atoms)' % j)
		activeTraj.setAtoms(activeSystem.select(('chindex %i name CA' % j))) 
		dataSingleChain['ca'] = calculateRMSD(activeSystem,activeTraj,indexArray)
		
		dataChains[j] = dataSingleChain

	return dataFull,dataChains,dataCustom

def getResidueData(activeSystem,activeTraj,indexArray,mode):

	numFrames = activeTraj.numFrames()
	numResidues = activeSystem.numResidues()
	numChains = activeSystem.numChains()
	uniqueChids = np.unique(activeSystem.getChids())
	uniqueResnums = np.unique(activeSystem.getResnums())

	dataRMSD = ['']*numResidues
	dataRGYR = ['']*numResidues
	dataRMSF = ['']*numResidues
	dataResidues = {'backbone':[],
	'all':[],
	'ca':[]}

	if (mode == 'all') | (mode == 'RMSD'):

		## Calculate RMSD values for each residue.
		print 'Calculating RMSD of each residue'

		dataResidues['backbone'] = {'rmsd':dataRMSD,'rgyr':dataRGYR,'rmsf':dataRMSF}
		dataResidues['all'] = {'rmsd':dataRMSD,'rgyr':dataRGYR,'rmsf':dataRMSF}

		iterResidues = activeSystem.iterResidues()

		for m in range(numResidues):
			
			## Calculate based on 'all' atoms
			activeTraj.setAtoms(iterResidues.next())
			activeSelection = activeTraj.getAtoms()
			activeChid = np.unique(activeSelection.getChids())
			activeResnum = np.unique(activeSelection.getResnums())
			print ('Calculating RMSD for chain %s residue %i (all atoms)' % (activeChid[0],activeResnum[0]))
			dataSingleResidue = calculateRMSD(activeSystem,activeTraj,indexArray)
		
			dataResidues['all']['rmsd'][m] = dataSingleResidue['rmsd']
			dataResidues['all']['rgyr'][m] = dataSingleResidue['rgyr']

			## Calculate based on 'backbone' atoms
			activeTraj.setAtoms(activeSystem.select('chid %s and resnum %i and backbone' % (activeChid[0],activeResnum[0])))
			print ('Calculating RMSD for chain %s residue %i (backbone atoms)' % (activeChid[0],activeResnum[0]))
			dataSingleResidue = calculateRMSD(activeSystem,activeTraj,indexArray)

			dataResidues['backbone']['rmsd'][m] = dataSingleResidue['rmsd']
			dataResidues['backbone']['rgyr'][m] = dataSingleResidue['rgyr']

	if (mode == 'all') | (mode == 'RMSF'):

		## Get RMSF values for each residue.
		print 'Calculating RMSF of each residue'

		dataResidues['ca'] = {'rmsf':dataRMSF};

		activeEnsemble = activeTraj[:]

		### !!! VERY IMPORTANT: Superpose the trajectory ensemble instance before all other calculations.'
		activeEnsemble.superpose()

		## Calculate RMSF based on carbon alpha atoms of residues.
		activeEnsemble.setAtoms(activeSystem.calpha)
		dataResidues['ca']['rmsf'] = activeEnsemble.getRMSFs()

	return dataResidues

def calculateRMSD(activeSystem,activeTraj,indexArray):
	
	rgyr = np.zeros(len(indexArray))
	rmsd = np.zeros(len(indexArray))
	
	activeTraj.reset()
		
	for k in range(len(indexArray)-1):
		
		# Calculate rmsd and rgyr over time for this chunk
		activeFrame = activeTraj.getFrame(indexArray[k])
		activeFrame.superpose()
		rmsd[k] = activeFrame.getRMSD()
		rgyr[k] = calcGyradius(activeFrame)

	data = {'rmsd': rmsd,'rgyr': rgyr}
	
	return data

def write2file(dataTraj,traj_tuple):

	now = datetime.datetime.now()

	# Reload system and trajectory into the memory.
	activeSystem = parsePDB(traj_tuple[0])
	activeTraj = Trajectory(traj_tuple[1])
	activeTraj.link(activeSystem)
	activeSystem = activeTraj.getLinked()
	numChains = activeSystem.numChains()
	numFrames = activeTraj.numFrames()
	numResidues = activeSystem.numResidues()

	# # Get file name of the trajectory.
	fileNameTraj = activeTraj.getFilenames()[0]

	if dataTraj['full']!='':

		# Open a file for writing time-based values (RMSD and RGYR versus time)
		fileNameTime = ('%s_RMSD_%i_%i_%i_%i%i%i.txt' % (fileNameTraj,now.day,now.month,now.year,now.hour,now.minute,now.second))
		print ('Writing the results to file %s...' % fileNameTime)
		f = open(fileNameTime,'w')

		# Start writing the output of dataTraj['full']
		f.write(fileNameTraj)
		f.write('\n')

		f.write('\t%s' % 'full')
		f.write('\n')
		f.write('\t%s\t\t%s\t' % ('all','ca'))
		f.write('\n')
		f.write('%s\t%s\t%s\t%s\t%s' % ('frame','rmsd','rgyr','rmsd','rgyr'))
		f.write('\n')

		for i in range(activeTraj.numFrames()):
			allRMSD = dataTraj['full']['all']['rmsd'][i]
			allRGYR = dataTraj['full']['all']['rgyr'][i]
			caRMSD = dataTraj['full']['ca']['rmsd'][i]
			caRGYR = dataTraj['full']['ca']['rgyr'][i]
			f.write('%s\t%s\t%s\t%s\t%s' % (str(i),str(allRMSD),str(allRGYR),str(caRMSD),str(caRGYR)))
			f.write('\n')

		f.close()

	if dataTraj['chains']!='':

		for i in range(numChains):

			# Open files for writing time-based values based on individual chains.
			fileNameTimeChain = ('%s_RMSD_chain%i_%i_%i_%i_%i%i%i.txt' % (fileNameTraj,i,now.day,now.month,now.year,now.hour,now.minute,now.second))
			print ('Writing the results of file %s...' % (fileNameTimeChain))
			f = open(fileNameTimeChain,'w')

			# Start writing the output of dataTraj['chains']
			f.write(fileNameTraj)
			f.write('\n')

			f.write('\t%s\t%i' % ('chain',i))
			f.write('\n')
			f.write('\t%s\t\t%s\t' % ('all','ca'))
			f.write('\n')
			f.write('%s\t%s\t%s\t%s\t%s' % ('frame','rmsd','rgyr','rmsd','rgyr'))
			f.write('\n')

			for m in range(activeTraj.numFrames()):
				allRMSD = dataTraj['chains'][i]['all']['rmsd'][m]
				allRGYR = dataTraj['chains'][i]['all']['rgyr'][m]
				caRMSD = dataTraj['chains'][i]['ca']['rmsd'][m]
				caRGYR = dataTraj['chains'][i]['ca']['rgyr'][m]
				f.write('%s\t%s\t%s\t%s\t%s' % (str(m),str(allRMSD),str(allRGYR),str(caRMSD),str(caRGYR)))
				f.write('\n')

			f.close()

	if dataTraj['residues']!='':

		if dataTraj['residues']['backbone']:

			## Open a file for writing residue-based RMSD values on backbones
			fileNameResRMSDbackbone = ('%s_RMSD_residue_backbone_%i_%i_%i_%i%i%i.txt' % (fileNameTraj,now.day,now.month,now.year,now.hour,now.minute,now.second))
			print ('Writing the results to file %s...' % fileNameResRMSDbackbone)
			f = open(fileNameResRMSDbackbone,'w')

			## Start writing the output of RMSD values from dataTraj['residues']
			f.write(fileNameResRMSDbackbone)
			f.write('\n')

			f.write('%s%s' % ('backbone','residues'))
			f.write('\n')

			residueNumArray = np.linspace(0,numResidues,numResidues)

			f.write('%s\t' % 'frame')

			for i in range(numResidues):
				
				f.write('%i\t' % (residueNumArray[i]))

			f.write('\n')

			for i in range(numFrames):

				f.write('%i\t' % i)

				for m in range(numResidues):

					f.write('%f\t' % dataTraj['residues']['backbone']['rmsd'][m][i])

				f.write('\n')

			f.close()

		if dataTraj['residues']['ca']:

			## Open a file for writing residue-based RMSF values based on CA atoms.
			fileNameRes = ('%s_RMSF_%i_%i_%i_%i%i%i.txt' % (fileNameTraj,now.day,now.month,now.year,now.hour,now.minute,now.second))
			print ('Writing the results to file %s...' % fileNameRes)
			f = open(fileNameRes,'w')

			## Start writing the output of dataTraj['residues']['ca']['rmsf']
			f.write(fileNameRes)
			f.write('\n')

			f.write('\t%s' % 'residues')
			f.write('\n')
			f.write('\t\t\t\t%s' % 'ca')
			f.write('\n')
			f.write('%s\t%s\t%s\t%s\t%s' % ('residue','chain','residue (chain)','resname','rmsf'))
			f.write('\n')

			iterResidues = activeSystem.iterResidues()
			for i in range(activeSystem.numResidues()):

				activeResidue = iterResidues.next()
				activeChid = np.unique(activeResidue.getChids())
				activeChid = activeChid[0]
				activeResname = np.unique(activeResidue.getResnames())
				activeResname = activeResname[0]
				activeResnum = activeResidue.getResnum()
				caResidueRMSF = dataTraj['residues']['ca']['rmsf'][i]
				f.write('%s\t%s\t%s\t%s\t%s' % (str(i),activeChid,str(activeResnum),activeResname,str(caResidueRMSF)))
				f.write('\n')

			f.close()

def getDistance(traj_tuple,sel1,sel2,toPlot=True):

	# Load the trajectory and system as usual.
	activeTraj = Trajectory(traj_tuple[1])
	activeSystem = parsePDB(traj_tuple[0])
	activeTraj.link(activeSystem)
	activeTraj.setCoords(activeTraj.getFrame(0)) # Set reference coordinates to frame zero!!!
	activeTraj.setAtoms(activeSystem.select('name CA'))

	numFrames = activeTraj.numFrames()

	distance = np.zeros(numFrames)

	for i in range(0,numFrames):
		activeFrame = activeTraj.getFrame(i)
		activeFrame.superpose()
		activeAtoms = activeFrame.getAtoms()
		distance[i] = calcDistance(activeAtoms.select(sel1),activeAtoms.select(sel2))

	if toPlot == True:

		plt.plot(distance)
		plt.show()

def getHydrogenBonds(traj_tuple,numCores=multiprocessing.cpu_count(),filterByChain='',toTxt=True,toPickle=True,noWarning=False):
	
	# Get the size of the DCD file. Warn the user, we need a disk space three times larger than this size!.
	statinfo = os.stat(traj_tuple[1])
	sizeMB = statinfo.st_size/1000000
	print 'The size of the trajectory file you supplied is %s MB.' % sizeMB
	print 'Mind that we need around three times larger free disk space than this (around %i MB).\nPlease make sure that you have available disk space before proceeding.' % (3.*sizeMB)
	if noWarning == True:
		proceed_check = 'yes'
	else:
		proceed_check = raw_input('Proceed ? (Yes/No): ')

	if proceed_check == 'yes' or proceed_check == 'Yes' or proceed_check == 'Y' or proceed_check == 'y':
		# Split the DCD into a sub-folder.
		directoryName = dcdTools.splitDCD(traj_tuple)

		# Run batchPqr on generated PDB files.
		os.chdir(directoryName)
		pqrTools.batchPqr(directoryName,noCores=numCores)

		# Parse the output PQR files.
		pqrFileNames = glob.glob('*.pqr')
		pqrFileName = pqrFileNames[0]
		uniqueContacts = pqrTools.parsePqrOutput(pqrFileName,'*.pdb.hbond')
		os.chdir('..')

		# Filter the uniqueContacts if user provided a chain.
		if filterByChain != '':
			uniqueContactsFilterByChain = pqrTools.filterUniqueContacts(uniqueContacts,filterByChain,toTxt=False,toPickle=False)
			fileNameFilterByChain = '%s_hbonds_chain_%s' % (traj_tuple[1],filterByChain)
		
		fileName = '%s_hbonds' % traj_tuple[1]
			
		# Write the contacts to disk
		# dcdFileNameNoExt, dcdFileNameExt = os.path.splitext(traj_tuple[1])
		
		if toTxt == True:
			pqrTools.saveUniqueContacts(uniqueContacts,fileName,fileType='txt')
			if filterByChain != '':
				pqrTools.saveUniqueContacts(uniqueContactsFilterByChain,fileNameFilterByChain,fileType='txt')

		if toPickle == True:
			pqrTools.saveUniqueContacts(uniqueContacts,fileName,fileType='pickle')
			if filterByChain != '':
				pqrTools.saveUniqueContacts(uniqueContactsFilterByChain,fileNameFilterByChain,fileType='pickle')			

		# Remove the splitDCD folder.
		shutil.rmtree(directoryName)

		if filterByChain != '':
			return uniqueContacts, uniqueContactsFilterByChain 
		else:
			return uniqueContacts

	else:
		print 'Cancelled.'