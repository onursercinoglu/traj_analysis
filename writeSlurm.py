#!/usr/bin/env python

# IMPORTS

import os, argparse
from namdTools import *

# INPUT PARSING
parser = argparse.ArgumentParser(description='Quickly write a SLURM settings \
	file for queueing NAMD MD jobs, specifically configured for the TRGRID Grid\
	 Computing System using	specified input settings.')

parser.add_argument('conffilename',type=str,nargs=1,
	help='Name of the NAMD conf file for which the SLURM settings file should\
	 be written.')

parser.add_argument('--p',type=str,nargs=1,default=['mercan'],
	help='SLURM partition setting')

parser.add_argument('--A',type=str,nargs=1,default=['osercinoglu'],
	help='User account name')

parser.add_argument('--J',type=str,nargs=1,default=['namd_job'],
	help='Job name')

parser.add_argument('--mail_user',type=str,nargs=1,
	default=['onursercin@gmail.com'],
	help='Email address for monitoring the job')

parser.add_argument('--N',type=int,nargs=1,default=[1],
	help='Number of nodes')

parser.add_argument('--ntasks',type=int,nargs=1,default=[24],
	help='Number of cores')

parser.add_argument('--time',type=str,nargs=1,default=[False],
	help='Requested running time')

parser.add_argument('--workdir',type=str,nargs=1,default=[os.getcwd()],
	help='Working directory')

args = parser.parse_args()

# METHODS

def writeSlurm(confFileName,p,A,J,mail_user,N,ntasks,time,workdir):

	confFileTitle = os.path.splitext(confFileName)[0]
	
	slurmFileName = ('%s.slurm' % confFileTitle)

	if p == 'cuda' and ntasks > 16:
		ntasks = 16

	if time==False:
		if p=='cuda':
			time='15-00:00:00'
		elif p == 'single':
			time='8-00:00:00'
		elif p == 'short':
			time='8:00:00'
		elif p == 'mid1' or p == 'mid2':
			time='8-00:00:00'
			if ntasks > 16:
				ntasks = 16
		elif p == 'long':
			time='15-00:00:00'
		elif p == 'mercan':
			time='15-00:00:00'

	f = open(slurmFileName,'w')
	f.write('#!/bin/bash')
	f.write('\n')
	f.write('#SBATCH -p %s' % p)
	f.write('\n')
	f.write('#SBATCH -A %s' % A)
	f.write('\n')
	f.write('#SBATCH -J %s' % J)
	f.write('\n')
	f.write('#SBATCH -N %s' % N)
	f.write('\n')
	f.write('#SBATCH --ntasks %s' % ntasks)
	f.write('\n')
	if p=='cuda':
		f.write('#SBATCH --gres=gpu:2')
		f.write('\n')
	f.write('#SBATCH --time=%s' % time)
	f.write('\n')
	f.write('#SBATCH --mail-type=ALL')
	f.write('\n')
	f.write('#SBATCH --mail-user=%s' % mail_user)
	f.write('\n')
	f.write('#SBATCH --output=%s/%s-slurm.out' % (workdir,J))
	f.write('\n')
	f.write('#SBATCH --error=%s/%s-slurm.err' % (workdir,J))
	f.write('\n')
	f.write('\n')
	f.write('# Load required modules')
	f.write('\n')
	if p=='cuda':
		f.write('module load centos6.4/app/namd/2.9-multicore-cuda')
		f.write('\n')
	else:
		f.write('module load centos6.4/app/namd/2.9-multicore')
		f.write('\n')
	f.write('\n')
	f.write('# Set working directory')
	f.write('\n')
	f.write('WORKDIR=%s' % workdir)
	f.write('\n')
	f.write('\n')
	f.write('### PRODUCTION STEP ###')
	f.write('\n')
	f.write('# Set input file')
	f.write('\n')
	f.write('INFILE=${WORKDIR}/%s' % confFileName)
	f.write('\n')
	f.write('\n')
	f.write('# Set output file')
	f.write('\n')
	f.write('OUTFILE=${WORKDIR}/%s.log' % confFileTitle)
	f.write('\n')
	f.write('\n')
	f.write('# Set standart error file')
	f.write('\n')
	f.write('ERRFILE=${WORKDIR}/%s.err' % confFileTitle)
	f.write('\n')
	f.write('\n')
	f.write('echo "Started production!"')
	f.write('\n')
	f.write('date')
	f.write('\n')
	f.write('\n')
	if p=='cuda':
		f.write(r'$NAMD_DIR/namd2 +p $SLURM_NTASKS +idlepoll +devices 0,1 $INFILE > $OUTFILE')
		f.write('\n')
	else:
		f.write(r'$NAMD_DIR/namd2 +p $SLURM_NTASKS +idlepoll $INFILE > $OUTFILE')
		f.write('\n')
	f.write('\n')
	f.write('echo "Finished production!"')
	f.write('\n')
	f.write('date')
	f.close()

	print ('Successfully written slurm configuration file %s.' % slurmFileName)

# CALLS

writeSlurm(args.conffilename[0],p=args.p[0],A=args.A[0],J=args.J[0],
	mail_user=args.mail_user[0],N=args.N[0],ntasks=args.ntasks[0],
	time=args.time[0],workdir=args.workdir[0])