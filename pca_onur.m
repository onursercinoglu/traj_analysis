function varargout = pca_onur(varargin)

%% Modified on: 03.07.2014 by Onur Serçinoğlu
%% Parse the input parameters.
p = inputParser;
p.addRequired('dcdname',@(x)ischar(x));
p.addRequired('res2read',@(x)isnumeric(x));
p.addParamValue('varThreshold',0.8);
p.addParamValue('modeThreshold',[]);
p.addParamValue('toFigure',false);

p.parse(varargin{:});

dcd_filename = p.Results.dcdname;
res2read = p.Results.res2read;
varThreshold = p.Results.varThreshold;
modeThreshold = p.Results.modeThreshold;
toFigure = p.Results.toFigure;

%% Read the dcd.
dcd = readdcd(dcd_filename,res2read);

%% Do the PCA.
frameCount =size(dcd,1);
alphaCount = size(dcd,2)/3;
[eig_vec,projected_pc,eig_val]=princomp(dcd);
variance=eig_val./sum(eig_val);
cumulative_variance=cumsum(eig_val)./sum(eig_val);

all_pc=zeros(alphaCount,3,alphaCount*3);
for j=1:alphaCount*3
    for i=1:alphaCount
        all_pc(i,:,j)= [eig_vec((3*i-2),j) eig_vec((3*i-1),j) eig_vec((3*i),j)];
    end
end

% Get modes that explain more than varThreshold.
if isempty(modeThreshold);
    for i=1:size(cumulative_variance,1)
        if cumulative_variance(i,1)>varThreshold
            mode_sel=i;
            break
        end
    end
else % If modeThreshold is specified, ignore mode_sel and take that mode Threshold.
    mode_sel = modeThreshold;
end

% Calculate cross-correlations.
cross_pc=zeros(alphaCount,alphaCount);
cross_mode=zeros(alphaCount,alphaCount);
for m=1:mode_sel
    for i=1:alphaCount
        for j=1:alphaCount
            cross_pc(i,j,m)=dot(all_pc(i,:,m),all_pc(j,:,m))/(norm(all_pc(i,:,m))*norm(all_pc(j,:,m)));       
        end     
    end
    cross_mode=cross_mode+cross_pc(:,:,m).*eig_val(m)/sum(eig_val(1:mode_sel));
end 

if toFigure == true

	%% Generate figures.
	figure; 
	imagesc(cross_mode, [-1 1]);
	set(gca,'YDir','normal');caxis([-1 1]);colorbar;
	xlabel('residue number','Fontsize',15)
	ylabel('residue number','Fontsize',15)
	set(gca,'XTick',[]);set(gca,'YTick',[]);
	axis square;colorbar;caxis([-1 1])
	set(gca,'XTick',(0:20:alphaCount));
	set(gca,'YTick',(0:20:alphaCount));
	set(gcf, 'Color', 'w')
	%figure_label_cutted(figure1)
	filename_suffix = [num2str(res2read(1)) '_' num2str(res2read(end))];
	filename = [dcd_filename '_' filename_suffix];
	title(['Cross Correlation of ',filename,' from PCA'], 'Fontsize',12,'interpreter','none')

	%% Save plots to .fig files.

	saveas(gcf,[filename '_PCA_Cross_Corr.fig']);
	save([filename '_PCA_Cross_Corr.mat'],'eig_vec', 'projected_pc', 'eig_val','cross_mode')

end

%% Return output according to nargout
if nargout == 1
	varargout = {cross_mode};
elseif nargout == 2
	varargout = {cross_mode,all_pc};
elseif nargout == 4
	varargout = {cross_mode,all_pc,cross_pc,dcd};  

end
