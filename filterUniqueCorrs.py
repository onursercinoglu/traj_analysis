#!/usr/bin/env python

# IMPORTS
import argparse
import numpy as np
from natsort import natsort
import re
import copy

# INPUT PARSING
parser = argparse.ArgumentParser(description='Removes duplicates from energy correlations \
	output file from getEnergyCorrs.py')

parser.add_argument('corrfile',type=str,nargs=1,help='Name of the corresponding output \
	file produced by getEnergyCorrs.py')

parser.add_argument('--outfile',type=str,nargs=1,default=['energyCorrsFiltered.dat'],
	help='If specified, the results will be written into OUTFILE')

args = parser.parse_args()

# METHODS
def filterUniqueCorrs(corrFile,outFile):

	dataList = list()

	corrFile = open(corrFile,'r')

	lines = corrFile.readlines()

	corrFile.close()

	for line in lines:
		if not (line.startswith('#') or line.startswith('R-squared')):

			# Get rid of newline and tab chars.
			dataline = line.strip('\n').split('\t')

			# Parse source-target pairs properly and sort them.
			sourcePairMatch = re.search('(\d+)-(\d+)',dataline[1])
			sourcePair = sourcePairMatch.groups()
			sourcePair = map(int,sourcePair)
			
			targetPairMatch = re.search('(\d+)-(\d+)',dataline[2])
			targetPair = targetPairMatch.groups()
			targetPair = natsort.natsorted(map(int,targetPair))
			
			sourceTargetList = [sourcePair,targetPair]
			sourceTargetList = sourceTargetList

			dataline = [float(dataline[0]),sourceTargetList]

			dataList.append(dataline)

	# Uniquify the list
	uniqueData = list()

	for item in dataList:

		if not item in uniqueData:
			uniqueData.append(item)


	uniqueCorrFile = open(outFile,'w')
	uniqueCorrFile.write('R2\tSource1\tSource2\tTarget1\tTarget2\n')

	for item in uniqueData:
		uniqueCorrFile.write('%.2f\t%i\t%i\t%i\t%i\n' % (item[0],
			item[1][0][0],item[1][0][1],item[1][1][0],item[1][1][1]))

# CALLS
filterUniqueCorrs(args.corrfile[0],outFile=args.outfile[0])