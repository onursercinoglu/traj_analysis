import re
import numpy as np
import copy
from prody import *
import os
import csv
import time
import datetime
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import getpass
import subprocess
import multiprocessing
import glob
import networkx as nx
import pickle
from natsort import natsorted
import pandas

class namdConf(object):

	def __init__(self):

		self.description ='A NAMD configuration file'
		self.structure = 'ionized.psf'
		self.coordinates = 'ionized.pdb'
		self.velocities = ''
		self.inputName = 'input'
		self.outputName = 'output'
		self.binCoordinates = '$inputname.restart.coor'
		self.binVelocities = '$inputname.restart.vel'
		self.extendedSystem = '$inputname.restart.xsc'
		self.firsttimestep = 0
		self.paraTypeCharmm = 'on'
		self.parameters = 'par_all27_prot_lipid_na.inp'
		self.temperature = 310
		self.wrapWater = 'on'
		self.wrapAll = 'off'
		self.exclude = 'scaled1-4'
		self.oneFourScaling = '1.0'
		self.cutoff = '12.'
		self.switching = 'on'
		self.switchdist = '10.'
		self.pairlistdist = '14.'
		self.margin = '2.0'
		self.timestep = '2.0'
		self.rigidBonds = 'all'
		self.nonbondedFreq = 1
		self.fullElectFrequency = 2
		self.stepspercycle = 10
		self.PME = 'yes'
		self.PMEGridSpacing = 1.0
		self.PMEGridSizeX = ''
		self.PMEGridSizeY = ''
		self.PMEGridSizeZ = ''
		self.langevin = 'on'
		self.langevinDamping = 5
		self.langevinTemp = self.temperature
		self.langevinHydrogen = 'no'
		self.cellBasisVector1 = ''
		self.cellBasisVector2 = ''
		self.cellBasisVector3 = ''
		self.cellOrigin = ''
		self.useGroupPressure = 'yes'
		self.useFlexibleCell = 'no'
		self.useConstantArea = 'no'
		self.langevinPiston = 'on'
		self.langevinPistonTarget = 1.01325
		self.langevinPistonPeriod = '100.'
		self.langevinPistonDecay = '50.'
		self.langevinPistonTemp = '310.'
		self.restartFreq = 500
		self.dcdFreq = 500
		self.xstFreq = 500
		self.outputEnergies = 500
		self.outputPressure = 500
		self.reinitvels = self.temperature
		self.mode = 'min' # can be 'min', 'run' or 'min-run'
		self.setMatches = ''
		self.restart = True # can be True or False, if true, restart files will be used, if false, restart files will not be used.
		self.minimize = 10000
		self.run = 500000

	def readConf(self,confFileName):

		setMatches = dict()
		f = open(confFileName,'r')
		data = f.readlines()
		for line in data:
			line = line.strip()
			if line.startswith('#'):
				continue
			matches = re.search(r'(\S+)\s+(\S+\s*\S*\s*\S*\s*)',line,re.IGNORECASE) # re.search will only keep the first match, and that is good for our purpose.
			if matches:
				parameter = matches.group(1)
				value = matches.group(2)

				if parameter == 'set':
					setVars = re.search(r'(\S+)\s+(\S+)',value) # matching variables declared with 'set'
					if setVars:
						setVarName = setVars.group(1)
						setVarValue = setVars.group(2)
						setMatches[setVarName] = setVarValue
				else:
					setattr(self,parameter,value)

		setattr(self,'setMatches',setMatches)

		f.close()

	def setCellBasisVectorOrigin(self):

		coords = parsePDB(self.coordinates)
		minX = min(coords.getCoords()[:,0])
		minY = min(coords.getCoords()[:,1])
		minZ = min(coords.getCoords()[:,2])
		maxX = max(coords.getCoords()[:,0])
		maxY = max(coords.getCoords()[:,1])
		maxZ = max(coords.getCoords()[:,2])
		meanX = np.mean(coords.getCoords()[:,0])
		meanY = np.mean(coords.getCoords()[:,1])
		meanZ = np.mean(coords.getCoords()[:,2])

		self.cellBasisVector1 = ' '.join([('%.2f' % abs(maxX-minX)),'0','0'])
		self.cellBasisVector2 = ' '.join(['0',('%.2f' % abs(maxY-minY)),'0'])
		self.cellBasisVector3 = ' '.join(['0','0',('%.2f' % abs(maxZ-minZ))])
		self.cellOrigin = ' '.join([('%.2f' % meanX),('%.2f' % meanY),('%.2f' % meanZ)])
		
	def writeConf(self,confFileName):

		f = open(confFileName,'w')
		
		# Set variables
		f.write(' '.join(['#'*40,'\n']))
		f.write('## SET VARIABLES\n')
		f.write(' '.join(['#'*40,'\n']))
		f.write('\n')
		if self.setMatches:
			print 'yes'
			for (key,value) in self.setMatches.iteritems():
				f.write('%-10s\t%s\t\t\t\t%-10s\n' % ('set',key,value))

		# Description block

		f.write(' '.join(['#'*40,'\n']))
		f.write(' '.join(['##',self.description,'\n']))
		f.write(' '.join(['#'*40,'\n']))
		f.write('\n')

		# Adjustable parameters block

		f.write(' '.join(['#'*40,'\n']))
		f.write('## ADJUSTABLE PARAMETERS\n')
		f.write(' '.join(['#'*40,'\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('structure',self.structure))
		f.write('%-10s\t\t\t\t%-10s\n' % ('coordinates',self.coordinates))
		f.write('%-10s\t\t\t\t%-10s\n' % ('outputName',self.outputName))
		f.write('\n')

		if self.restart:
			f.write(' '.join(['## Using restart files:','\n']))
			f.write('\n')
			f.write('%-10s\t\t\t%-10s\n' % ('set inputName',self.inputName))
			f.write('%-10s\t\t\t%-10s\n' % ('binCoordinates','$inputName.restart.coor'))
			f.write('%-10s\t\t\t%-10s\n' % ('binVelocities','$inputName.restart.vel'))
			f.write('%-10s\t\t\t%-10s\n' % ('extendedSystem','$inputName.restart.xsc'))
		else:
			f.write(' '.join(['## Not using restart files:','\n']))
			f.write('\n')
			f.write('%-10s\t\t\t%-10s\n' % ('set temperature',self.temperature))
			f.write('%-10s\t\t\t\t%-10s\n' % ('temperature','$temperature'))

		f.write('\n')
		f.write('%-10s\t\t\t%-10s\n' % ('firsttimestep',self.firsttimestep))
		f.write('\n')

		# Simulation parameters block

		f.write(' '.join(['#'*40,'\n']))
		f.write('## SIMULATION PARAMETERS\n')
		f.write(' '.join(['#'*40,'\n']))
		f.write('\n')
		f.write(' '.join(['## Input','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t%-10s\n' % ('paraTypeCharmm',self.paraTypeCharmm))
		f.write('%-10s\t\t\t\t%-10s\n' % ('parameters',self.parameters))
		f.write('\n')
		f.write(' '.join(['## Force-Field','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('exclude',self.exclude))
		f.write('%-10s\t\t\t\t%-10s\n' % ('1-4scaling',self.oneFourScaling))
		f.write('%-10s\t\t\t\t%-10s\n' % ('cutoff',self.cutoff))
		f.write('%-10s\t\t\t\t%-10s\n' % ('switching',self.switching))
		f.write('%-10s\t\t\t\t%-10s\n' % ('switchdist',self.switchdist))
		f.write('%-10s\t\t\t%-10s\n' % ('pairlistdist',self.pairlistdist))
		f.write('%-10s\t\t\t\t%-10s\n' % ('margin',self.margin))
		f.write('\n')
		f.write(' '.join(['## Integrator','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('timestep',self.timestep))
		f.write('%-10s\t\t\t\t%-10s\n' % ('rigidBonds',self.rigidBonds))
		f.write('%-10s\t\t\t%-10s\n' % ('nonbondedFreq',self.nonbondedFreq))
		f.write('%-10s\t\t%-10s\n' % ('fullElectFrequency',self.fullElectFrequency))
		f.write('%-10s\t\t\t%-10s\n' % ('stepspercycle',self.stepspercycle))
		f.write('\n')
		f.write(' '.join(['## Constant Temperature Control','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('langevin',self.langevin))
		f.write('%-10s\t\t\t%-10s\n' % ('langevinDamping',self.langevinDamping))
		f.write('%-10s\t\t\t%-10s\n' % ('langevinTemp',self.langevinTemp))
		f.write('%-10s\t\t%-10s\n' % ('langevinHydrogen',self.langevinHydrogen))
		f.write('\n')
		# paramValueDict = vars(self)
		# for key, value in paramValueDict.iteritems():
		# 	f.write(('%-10s\t\t\t%10s\n' % (key,value)))

		if self.restart == False: # if restart is False, then we must specify periodic boundary conditions from properties.
			f.write(' '.join(['## Periodic Boundary Conditions','\n']))
			f.write('\n')
			f.write('%-10s\t\t%-10s\n' % ('cellBasisVector1',self.cellBasisVector1))
			f.write('%-10s\t\t%-10s\n' % ('cellBasisVector2',self.cellBasisVector2))
			f.write('%-10s\t\t%-10s\n' % ('cellBasisVector3',self.cellBasisVector3))
			f.write('%-10s\t\t\t\t%-10s\n' % ('cellOrigin',self.cellOrigin))
			f.write('%-10s\t\t\t\t%-10s\n' % ('wrapAll',self.wrapAll))
		else:
			f.write('%-10s\t\t\t\t%-10s\n' % ('wrapWater',self.wrapWater))
			f.write('%-10s\t\t\t\t%-10s\n' % ('wrapAll',self.wrapAll))

		f.write('\n')
		f.write(' '.join(['## PME (for full-system periodic electrostatics)','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('PME',self.PME))
		f.write('%-10s\t\t\t%-10s\n' % ('PMEGridSpacing',self.PMEGridSpacing))

		if float(self.PMEGridSpacing) != 1: # If PMEGridSpacing is not equal to 1.0, specify grid sizes from properties.
			f.write('\n')
			f.write(' '.join(['## Manual grid definitions','\n']))
			f.write('%-10s\t\t\t%-10s\n' % ('PMEGridSizeX',self.wrapAll))
			f.write('%-10s\t\t\t%-10s\n' % ('PMEGridSizeY',self.wrapAll))
			f.write('%-10s\t\t\t%-10s\n' % ('PMEGridSizeZ',self.wrapAll))

		f.write('\n')
		f.write(' '.join(['## Constant Pressure Control (variable volume)','\n']))
		f.write('\n')
		f.write('%-10s\t\t%-10s\n' % ('useGroupPressure',self.useGroupPressure))
		f.write('%-10s\t\t\t%-10s\n' % ('useFlexibleCell',self.useFlexibleCell))
		f.write('%-10s\t\t\t%-10s\n' % ('useConstantArea',self.useConstantArea))
		f.write('%-10s\t\t\t%-10s\n' % ('langevinPiston',self.langevinPiston))
		f.write('%-10s\t%-10s\n' % ('langevinPistonTarget',self.langevinPistonTarget))
		f.write('%-10s\t%-10s\n' % ('langevinPistonPeriod',self.langevinPistonPeriod))
		f.write('%-10s\t\t%-10s\n' % ('langevinPistonDecay',self.langevinPistonDecay))
		f.write('%-10s\t\t%-10s\n' % ('langevinPistonTemp',self.langevinPistonTemp))
		f.write('\n')
		f.write(' '.join(['## Output Frequencies','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('restartFreq',self.restartFreq))
		f.write('%-10s\t\t\t\t%-10s\n' % ('dcdFreq',self.dcdFreq))
		f.write('%-10s\t\t\t\t%-10s\n' % ('xstFreq',self.xstFreq))
		f.write('%-10s\t\t\t%-10s\n' % ('outputEnergies',self.outputEnergies))
		f.write('%-10s\t\t\t%-10s\n' % ('outputPressure',self.outputPressure))
		f.write('\n')

		# Execution
		f.write(' '.join(['#'*40,'\n']))
		f.write('## EXECUTION SCRIPT\n')
		f.write(' '.join(['#'*40,'\n']))
		f.write('\n')

		if self.mode == 'min':
			f.write(' '.join(['## Minimization:','\n']))
			f.write('\n')
			f.write('%-10s\t\t\t\t%-10s\n' % ('minimize',self.minimize))
			f.write('%-10s\t\t\t\t%-10s\n' % ('reinitvels',self.reinitvels))
		elif self.mode == 'run':
			f.write(' '.join(['## MD Run:','\n']))
			f.write('\n')
			f.write('%-10s\t\t\t\t%-10s\n' % ('run',self.run))
		elif self.mode == 'min-run':
			f.write(' '.join(['## Minimization & MD Run:','\n']))
			f.write('\n')
			f.write('%-10s\t\t\t\t%-10s\n' % ('minimize',self.minimize))
			f.write('%-10s\t\t\t\t%-10s\n' % ('reinitvels',self.reinitvels))
			f.write('%-10s\t\t\t\t%-10s\n' % ('run',self.run))

		f.close()

		print ('conf file %s was written with success.' % confFileName)

def continueConf(confFileName,finalTimeStep,outputName=False,mode='run'):

	prevConf =  namdConf()
	prevConf.readConf(confFileName)

	# Here scan for the .restart.xsc with the outputname of the prevConf.
	targetTimeStep = prevConf.run

	# Here get the outputname, then read the last time step from its restart.xsc file.
	prevOutputName = prevConf.outputName
	if prevOutputName.startswith('$'):
		matches = re.search(r'\$(\S+)',prevOutputName)
		setVarName = matches.group(1)
		prevOutputName = prevConf.setMatches[setVarName]

	f = open('.'.join([prevOutputName,'restart','xsc']),'r')
	inputData = f.readlines()

	if len(inputData) > 2:
		lastLine = inputData[2].split()
		lastTimeStep = int(lastLine[0])
	else:
		print 'Your restart.xsc file is corrupted.'
	
	f.close()

	# Modify parameters of the namdConf object according to the parameters given.
	newConf = copy.deepcopy(prevConf)
	newConf.restart = True
	newConf.inputName = prevOutputName
	newConf.firsttimestep = lastTimeStep
	runTimeSteps = finalTimeStep - lastTimeStep

	if mode == 'min':
		newConf.mode = 'min'
		newConf.minimize = runTimeSteps
	elif mode == 'run':
		newConf.mode = 'run'
		newConf.run = runTimeSteps

	if outputName == False:
		outputName = ('restart_%s_%s' % (lastTimeStep,finalTimeStep))

	newConf.outputName = outputName

	for (key,value) in newConf.setMatches.iteritems():
		inputMatch = re.search(r'inputname',key,re.IGNORECASE)
		if inputMatch:
			newConf.setMatches[key] = newConf.inputName
		outputMatch = re.search(r'outputname',key,re.IGNORECASE)
		if outputMatch:
			newConf.setMatches[key] = newConf.outputName

	newConf.writeConf('.'.join([outputName,'conf']))

def listXSC(folderName=os.getcwd()):

	# This method scans a given folder for .xsc files, reads and returns the last timesteps of simulations as a .csv file.
	fw = open(os.path.join(folderName,'xscList.csv'),'wb')
	writer = csv.writer(fw,delimiter='\t')
	writer.writerow(['Directory','File Name','Last Time Step'])
	for root, dirs, files in os.walk(folderName):
		for filename in files:
			if (filename.endswith(('.xsc')) and len(re.findall('alanin.initial',filename)) == 0):
				fr = open(os.path.join(root,filename),'r')
				inputData = fr.readlines()
				if len(inputData) > 2:
					lastLine = inputData[2].split()
					lastTimeStep = lastLine[0]
					#print filename, lastTimeStep
					writer.writerow([root,filename,lastTimeStep])				
					#print '%s/%s\t%s' % (root,filename,lastTimeStep)
				fr.close()
	fw.close()
	print 'Success!!'
	print ('xscList.csv was created (or updated) in %s, please open it to see the last time steps of all .xsc files located in input directory' % folderName)

def monitorXSC(xscFileName,targetTimeStep,reportInterval=60,targetMail=''):

	# This method scans a given .xsc file continuously and reports the time steps reached in specified time intervals by sending emails to a specified email adress.
	
	# Prompt for gmail username and password if the user specifies targetMail
	if targetMail:
		print 'I need your gmail username and password since you have specified a target email adress.'
		userNameGmail = raw_input('Enter your Gmail username (including @gmail.com):')
		userPwdGmail = getpass.getpass(prompt='Enter your Gmail password:')

	# Start monitoring the .xsc file.
	print ('Monitoring %s now...' % xscFileName)
	stopFlag = False
	now = 0

	# Start a while loop for as long as the .xsc file is being updated (i.e. the simulation is running and hence stopFlag=False).
	while stopFlag==False:

		# Only check if more than a minute has passed since the last check.
		if (time.time() - now) > 60*reportInterval:
			now = time.time()
			xscFile = open(xscFileName,'r')
			inputData = xscFile.readlines()
		
			if len(inputData) > 2:
				lastLine = inputData[2].split()
				lastTimeStep = lastLine[0]
				print ('Last time step in %s is: %s' % (xscFileName,lastTimeStep))

			xscFile.close()

			# Send a quick progress report to targetMail.
			if targetMail:
				smtp = smtplib.SMTP('smtp.gmail.com:587')
				smtp.starttls()
				smtp.login(userNameGmail,userPwdGmail)
				msg = MIMEMultipart()
				msg['From'] = userNameGmail
				msg['To'] = targetMail
				now_datetime= datetime.datetime.now()
				hour = now_datetime.hour
				minute = now_datetime.minute
				day = now_datetime.day
				month = now_datetime.month
				year = now_datetime.year
				
				if int(lastTimeStep) >= targetTimeStep:
					msg['Subject'] = ('COMPLETED! %s has reached your specified final time step of %s' % (xscFileName,targetTimeStep))
					stopFlag = True # Stop monitoring since we have reached the targetTimeStep.
				else:
					msg['Subject'] = ('RUNNING: %s has reached time step %s' % (xscFileName,lastTimeStep))

				body = ('%s reached time step %s at %s:%s on %s-%s-%s' % (xscFileName,lastTimeStep,hour,minute,day,month,year))
				msg.attach(MIMEText(body,'plain'))
				text = msg.as_string()
				smtp.sendmail(userNameGmail,targetMail,text)

def getPerformance(outputName,lengthOfSimulation):

	#outputName: outputname of the namd simulation, must be specified by the user 
	#lengthOfSimulation: length of simulation, must be specified by the user (can be obtained with the command squeue)

	f = open('.'.join([outputName,'xst']),'r')
	inputData = f.readlines()

	if len(inputData) > 2:
		thirdLine = inputData[2].split()
		startTimeStep = int(thirdLine[0])
		lastLine = inputData[-1].split()
		lastTimeStep = int(lastLine[0])
		performance = (lastTimeStep - startTimeStep) / lengthOfSimulation
		print '%s has run at a rate of %i time steps per minute' % (outputName,performance)
		print '%s has run at a rate of %i time steps per hour' % (outputName,performance*60)
	else:
		print 'Your .xst file is corrupted. I am not able to calculate performance of simulation'
	
	f.close()

def extractEnergiesFromLog(logFileName,outputName=False):
	"""
	Reads in a .log file generated by namd, extracts energy lines (which start with 'ENERGY: ') and prints them into a file.

	Parameters
	----------
	logFileName: string, required
		a string which includes the name of the log file.
	outputName: False (boolean, default) or a string
		If False, then the generated output file name will be like: logFileName_energies.txt.
		If a string, then the generated output file name will be like: outputName

	Returns
	-------
	none

	Author
	------
	Onur Sercinoglu
	"""

	logFile = open(logFileName,'r')
	
	logFilePathNoExt, logFileNameExt = os.path.splitext(logFileName)

	if outputName == False:
		energyFile = open('%s_energies.txt' % logFilePathNoExt,'w')
	else:
		energyFile = open(outputName,'w')

	for line in logFile:
		if re.match('ENERGY: ',line):
			print >> energyFile, line

	print "Successfully written energy values into file %s" % energyFile

def getResidueEnergies(traj_tuple,numCores=1,sourceSel='protein',targetSel=False,iterCalc=False,
	skip=1,iterCutoff=20,outputFolder=os.getcwd(),toDat=True,compactDat=True,toPickle=False):
	"""
	!!!! toDat compactFile and toPickle arguments yet unfunctional!!!

	Extracts residue-based energy values from a DCD using namdenergy from VMD and writes them into
	files.
	
	Parameters
	----------
	traj_tuple : tuple or array, required
		a tuple or array including a PSF and DCD pair, in the form of 
		['psffilename.psf','dcdfilename.dcd']
	numCores : integer, 1 (default), optional
		determines the number of CPU cores to be employed.
		If 1, no parallel computation will be employed.
		If higher than 1,say n, then the computational workload will be distribued among 
		n processors for speed-up.
	sourceSel: string, optional
		determines a residue selection string in ProDy format.
		If not specified (default), all residues will be selected.
		If specified, residues specified in sourceSel will be taken into account and no calculation
		 will be made for any other
		residues.
	targetSel: string, optional
		determines a residue selection string in ProDy format. 
		requires the sourceSel to be specified as well, otherwise this argument is ignored.
		If not specified (default), the optional 'selection 2' argument in namdEnergy will be left 
		empty.
		If specified, the energy between sourceSel and targetSel residues will be calculated only. 
		The calculation mode differs
		depending on iterCalc argument as well.
	iterCalc: boolean, False (default), optional
		determines whether an iterative energy evaluation shall be made between sourceSel and 
		targetSel.
		If False (default), only the total energy between all sourceSel and targetSel residues are 
		calculated.
		If True, an iterative calculation is performed. First, the interaction energy between the 
		first residue in sourceSel and 
		the first residue in targetSel is calculated. Then, the interaction energy between the 
		second residue in sourceSel and the 
		first residue in targetSel is calculated. And so on.
	iterCutoff: integer, 20 (default), optional
		determines a cutoff range for residue-residue iterative energy calculations. 
		Only residues that are within the iterCutoff distance will be take into account in energy
		calculations.
	skip: integer, 1 (default), optional
		If specified, namdenergy.tcl will use this skip parameter, which defines the number of 
		frames in dcd to skip between each calculation.
	toDat: boolean, True (default), optional
		specifies whether energies areto be written into .dat file(s). 
		If True, then results will be saved into .dat files in tab-delimited format. Saving process
		 will also depend on the compactDat argument.
	compactDat: boolean, True (default), optional
		specifies whether (a) compact file(s) including all residue energies should be generated.
		this argument is only taken into account when toFile is also True.
		If True, compact results file(s) will be generated (including energies of all residues as 
			columns) for 
		each energy type included in the output files from energy calculation. The output files 
		from namdenergy.tcl are cleaned-up afterwards.
		If False, no compact results file is generated. The output files from namdenergy.tcl are 
		left as they are.
	toPickle: boolean, False (default), optional
		specifies whether the returning list is saved into a pickle file for later analysis.
		If False (default), nothing is done.
		If True, then a pickle file containing the returned results will be saved.

	Returns
	-------
	none 

	Author
	------
	Onur Sercinoglu
	"""

	# Get the file path and name for PSF (assuming it has the same file name as PDB)
	psfFilePathNoExt, pdbFilePathExt = os.path.splitext(traj_tuple[0])
	psfFileName = os.path.basename(traj_tuple[0])
	psfFileNameNoExt,_ = os.path.splitext(psfFileName)

	# Get the file path and name for DCD without extension
	dcdFilePathNoExt, dcdFilePathExt = os.path.splitext(traj_tuple[1])
	dcdFileName = os.path.basename(traj_tuple[1])
	dcdFileNameNoExt,_ = os.path.splitext(dcdFileName)
	
	# Load pdb with prody and get some useful numbers.
	system = parsePDB(traj_tuple[0])
	source = system.select(sourceSel)
	sourceCA = source.select('calpha')
	numSource = len(sourceCA)
	sourceResids = sourceCA.getResindices()
	sourceResnums = sourceCA.getResnums()
	sourceSegnames = sourceCA.getSegnames()

	allResidues = system.select('all')
	allResiduesCA = allResidues.select('calpha')
	numResidues = len(allResiduesCA)
	numTarget = numResidues

	# By default, targetResids are all residues.
	targetResids = np.arange(numResidues)
	
	# Get target selection residues, if provided:
	if targetSel:
		target = system.select(targetSel)

		## HERE WE MUST INSERT A CUTOFF FILTERING CODE TO REDUCE THE targetSel SELECTION 
		## BECAUSE TOO MANY TARGETSEL RESIDUES CAUSE A LOT OF UNNECESSARY COMPUTATION.

		targetCA = target.select('calpha')
		numTarget = len(targetCA)
		targetResids = targetCA.getResindices()

	if numCores == 1: # Then the user does not want multiple processors (too bad lol:))

		residChunk = [sourceResids[0],sourceResids[-1]]

		calcResidueEnergiesSingleCore(residChunk,psfFilePathNoExt,dcdFilePathNoExt,targetSel,
			targetResids,iterCalc,skip,iterCutoff)

	elif numCores > 1: # Way to go!!! ;)
		
		# Start a processor pool.
		pool = multiprocessing.Pool(numCores)

		# Create appropriate chunks of resids according to numCores and the size of sourceSel and 
		if numSource > numTarget or numSource == numTarget:
			residChunks = np.array_split(sourceResids,numCores)

		# Call the method getResidueEnergiesSingleCore multiple times with multiprocessing module.
			for residChunk in residChunks:
				pool.apply_async(calcResidueEnergiesSingleCore,args=(residChunk,psfFilePathNoExt,
					dcdFilePathNoExt,dcdFileNameNoExt,
					targetSel,targetResids,iterCalc,skip,iterCutoff,outputFolder))

		elif numTarget > numSource:
			residChunks = np.array_split(targetResids,numCores)

		# Call the method getResidueEnergiesSingleCore multiple times with multiprocessing module.
			for residChunk in residChunks:
				pool.apply_async(calcResidueEnergiesSingleCore,args=(sourceResids,psfFilePathNoExt,
					dcdFilePathNoExt,dcdFileNameNoExt,
					targetSel,residChunk,iterCalc,skip,iterCutoff,outputFolder))

		pool.close()
		pool.join()

	# Prepare to write: get the date:
	now = datetime.datetime.now()

	# Write into files. WARNING: Long if/elif/for stacks. I know it is not optimal.
	if iterCalc == False:
		# Parse the .dat files generated by namdenergy into a list.
		energyOutputFiles = parseEnergyOutput('%s_[^_]+_[^_]+_energies.dat' % dcdFilePathNoExt)
		
		# Get file names.
		fileKeys = energyOutputFiles.keys()
		fileKeys = natsorted(fileKeys)
		numKeys = len(fileKeys)

		# Get energy types from the first file.
		energyTypes = energyOutputFiles[fileKeys[0]].keys()

		# Write into .dat files.
		if toDat==True and compactDat==True:

			# Write into separate files according to energy types.
			for i in range(0,len(energyTypes)):

				# Get the curent energy type
				energyType = energyTypes[i]

				# If energyType is Time or Frame, then it is not an energy type. skip to next 
				# iteration.
				if energyType == 'Time' or energyType == 'Frame':
					continue
				
				# Print a header information.
				fileNameTime = '%s_sourceSel_%s_%i_%02d_%02d_%02d%02d%02d.dat' % \
				(dcdFileNameNoExt,energyType,now.year,now.month,now.day,now.hour,now.minute,
					now.second)
				print ('Writing energies to file %s...' % fileNameTime)
				f = open(fileNameTime,'w')
				f.write(fileNameTime)
				f.write('\n')
				f.write('This file lists %s energies for each residue in sourceSel' % energyType)
				f.write('\n')
				f.write('\n')

				# Print data headers.
				numFrames = len(energyOutputFiles[fileKeys[0]][energyType])

				titleString = 'Frame'

				for k in range(0,numKeys):
					# Get the residue segname and resid from file's name and assign it as column header.
					matches = re.match(('%s_([^_]+_[^_]+)_energies.dat' % dcdFileNameNoExt),
						fileKeys[k])
					activeColumnTitle = matches.groups()[0]
					titleString = titleString+'\t'+activeColumnTitle

				f.write(titleString)
				f.write('\n')

				# Print actual data.
				for l in range(0,numFrames):
					lineString = str(l)
					
					##########################################################
					##### SAME PROBLEM AS ABOVE!!! ###########################
					##########################################################

					for m in range(0,numKeys):
						lineString = lineString+'\t'+str(energyOutputFiles[fileKeys[m]][energyType][l])

					f.write(lineString)
					f.write('\n')

				f.close()

			# Clean up files, because compactDat is True.
			for n in range(0,numKeys):
				os.remove(fileKeys[n])

		if toPickle == True:
			print 'under construction'

	elif iterCalc == True:
		# Parse the .dat file generated by namdenergy into a list.
		energyOutputFiles = parseEnergyOutput('%s_[^_]+_[^_]+_[^_]+_[^_]+_energies.dat' % 
			dcdFileNameNoExt)

		# Get file names.
		fileKeys = energyOutputFiles.keys()
		fileKeys = natsorted(fileKeys)
		numKeys = len(fileKeys)

		# Get unique source residues.
		uniqueSources = list()
		for i in range(0,numKeys):
			matches = re.match(('%s_([^_]+_[^_]+)_[^_]+_[^_]+_energies.dat' % dcdFileNameNoExt),
				fileKeys[i])
			uniqueSources.append(matches.groups()[0])

		uniqueSources = np.unique(uniqueSources)

		# Get energy types from the first file.
		energyTypes = energyOutputFiles[fileKeys[0]].keys()

		# Write into .dat files
		if toDat == True and compactDat == True:

			# Write into separate files according to energy types and sourceSel residues.
			for i in range(0,len(energyTypes)):
				energyType = energyTypes[i]

				if energyType == 'Time' or energyType == 'Frame':
					continue

				for j in range(0,len(uniqueSources)):
					# Get active source residue's segname and resid.
					activeSourceResidue = uniqueSources[j]

					# Construct the file name.
					fileNameTime = '%s_%s_targetSel_%s_%i_%02d_%02d_%02d%02d%02d.dat' % \
					(dcdFileNameNoExt,activeSourceResidue,energyType,now.year,now.month,now.day,
						now.hour,now.minute,now.second)
					print ('Writing energies to file %s' % fileNameTime)

					f = open(fileNameTime,'w')
					f.write(fileNameTime)
					f.write('\n')
					f.write('This file lists %s energies between residue %s and targetSel residues' 
						% (energyType,activeSourceResidue))
					f.write('\n')
					f.write('\n')

					numFrames = len(energyOutputFiles[fileKeys[0]][energyType])


					#############!!!! CHANGE THIS !!!!##############################
					## HERE ITERATING OVER numKeys MIGHT BE A BAD IDEA BECAUSE FOR RES-RES PAIRS FOR
					## WHICH NO ENERGY WAS COMPUTED THERE WILL BE NO COLUMN AND COMPARING TWO DIFFERENT
					## .DAT FILES WILL BE PROBLEMATIC LATER ON.
					###############################################################

					# Find the files with the active source residues.
					activeSourceTargetResidues = list()
					for k in range(0,numKeys):
						matches = re.match(('%s_%s_([^_]+_[^_]+)_energies.dat' % (dcdFileNameNoExt,
							activeSourceResidue)),fileKeys[k])

						# If not match is found, continue with the next iterations of this for loop.
						if not matches:
							continue
						else:
							activeSourceTargetResidues.append(matches.groups()[0])

					numActiveSourceTargetResidues = len(activeSourceTargetResidues)

					titleString = 'Frame'
					for k in range(0,numActiveSourceTargetResidues):
						activeSourceTargetResidue = activeSourceTargetResidues[k]
						titleString = titleString+'\t'+activeSourceTargetResidue

					f.write(titleString)
					f.write('\n')

					for l in range(0,numFrames):
						lineString = str(l)

						for m in range(0,numActiveSourceTargetResidues):
							activeSourceTargetResidue = activeSourceTargetResidues[m]
							fileKey = ('%s_%s_%s_energies.dat' % (dcdFileNameNoExt,
								activeSourceResidue,activeSourceTargetResidue))
							lineString = lineString+'\t'+str(energyOutputFiles[fileKey][energyType][l])

						f.write(lineString)
						f.write('\n')

					f.close()

			# Clean up files, because compactDat is True.
			for n in range(0,numKeys):
				os.remove(fileKeys[n])

	# # Pack the energy outputs into a list.
	# energyOutputResidues = list()

	# return

	# for i in range(0,numResidues):
	# 	residueKey = '%s_%s_%i_energies.dat' % (dcdFilePathNoExt,segnames[i],resnums[i])
	# 	residueOutput = energyOutputFiles[residueKey]
	# 	energyOutputResidues.append(residueOutput)

	# 	# Delete the namdenergy output dat file if necessary.
	# 	if not toFile == True and compactFile == False:
	# 		os.remove(residueKey)

	# if toFile == True and compactFile == True: # Create a compact results file.
	# 	writeEnergies2file(energyOutputResidues,dcdFilePathNoExt,fileType='dat')

	# if toPickle == True:
	# 	writeEnergies2file(energyOutputResidues,dcdFilePathNoExt,fileType='pickle')

	# return energyOutputResidues

def calcResidueEnergiesSingleCore(residChunk,psfFilePathNoExt,dcdFilePathNoExt,dcdFileNameNoExt,
	targetSel,targetResids,iterCalc,skip,iterCutoff,outputFolder):
	"""
	Do not call this method directly.
	"""

	# Get the path of module, necessary when providing vmd the location of namdEnergyPerResidues.tcl. see below.
	module_path = os.path.dirname(os.path.realpath(__file__))

	# If targetSel and iterCalc are False:
	# calculate the total energy between individial residues of sourceSel and all the rest of the protein.
	# this is equivalent to calculating the total energy of each residue in sourceSel.
	if targetSel == False and iterCalc == False:
		subprocess.call('vmd -dispdev text -e %s/namdEnergyPerResidue.tcl -args %s.psf %s.dcd %i %i %s %s %i %i' % \
			(module_path,psfFilePathNoExt,dcdFilePathNoExt,skip,iterCutoff,outputFolder,dcdFileNameNoExt,residChunk[0],residChunk[-1]),shell=True)

	# If targetSel is provided (not False):
	# iterCalc is provided to namdEnergyPerResidue.
	# If iterCalc is False: calculate the non-bonded energy between individiual residues of sourceSel and all the targetSel residues.
	# If iterCalc is True: calculate the non-bonded energy between individiual residues of sourceSel and individiual residues of targetSel residues.
	elif targetSel != False:
		subprocess.call('vmd -dispdev text -e %s/namdEnergyPerResidue.tcl -args %s.psf %s.dcd %i %i %s %s %i %i %s %i %i' % \
			(module_path,psfFilePathNoExt,dcdFilePathNoExt,skip,iterCutoff,outputFolder,dcdFileNameNoExt,residChunk[0],residChunk[-1],iterCalc,targetResids[0],targetResids[-1]),shell=True)

def parseEnergyOutput(regExpEnergyOutputFiles):
	"""
	parses an output file of namdenergy.tcl or a regular expression which denotes a group of namdenergy.tcl output files, then returns the contents in a dict.

	Parameters
	----------
	regExpEnergyOutputFiles: string, required
		denotes an output file string or a regular expression which denotes a group of output files.

	Returns
	-------
	energyOutputs: dict
		a dictionary with file names as keys and dictionaries with appropriate keys from output files as values.

	Author
	------
	Onur Sercinoglu
	"""

	# Real all output files matching the regular expression given in regExpEnergyOutputFiles.
	fileNames = glob.glob('*')
	energyOutputFileNames = list()
	for i in range(0,len(fileNames)):
		matches = re.match(regExpEnergyOutputFiles,fileNames[i])
		if matches:
			energyOutputFileNames.append(matches.group())

	energyOutputFileNames.sort()

	# Start a dict for storing outputs.
	energyOutputs = dict()

	# Read in the files one by one
	for fileName in energyOutputFileNames:
		
		# Start a dict for storing output.
		energyOutput = dict()

		# Read in the first line (header) output file and count number of total lines.
		f = open(fileName,'r')
		lines = f.readlines()
		numLines = len(lines)
		header = lines[0].split()
		numTitles = len(header)

		# Close the file and reopen it with numpy's loadtxt, which is much more practical for numeric data.
		f.close()
		energies = np.loadtxt(fileName,skiprows=1) # Skipping the header row.

		# Assign each column into appropriate key's value in energyOutput dict.
		for i in range(0,numTitles):
			energyOutput[header[i]] = energies[:,i]

		# Append energyOutput into appropriate key's value in energOutputs dict.
		energyOutputs[fileName] = energyOutput

	return energyOutputs

def writeEnergies2file(energyOutputResidues,outputName='Energies',fileType='dat'):
	"""

	"""

	now = datetime.datetime.now()
	energyTypes = energyOutputResidues[0].keys()

	if fileType == 'dat':

		for i in range(0,len(energyTypes)):
			energyType = energyTypes[i]
			fileNameTime = '%s_%s_%i_%i_%i_%i%i%i.dat' % (outputName,energyType,now.day,now.month,now.year,now.hour,now.minute,now.second)
			print ('Writing energies to file %s...' % fileNameTime)
			f = open(fileNameTime,'w')
			f.write(fileNameTime)
			f.write('\n')

			f.write('This file lists the %s energies for each residue.' % energyType)
			f.write('\n')
			f.write('\n')

			numResidues = len(energyOutputResidues)
			numFrames = len(energyOutputResidues[0][energyType])

			titleString = 'Frame'
			for k in range(0,numResidues):
				titleString = titleString+'\t'+str(k)

			f.write(titleString)
			f.write('\n')

			for l in range(0,numFrames):
				lineString = str(l)
				for m in range(0,numResidues):
					lineString = lineString+'\t'+str(energyOutputResidues[m][energyType][l])

				f.write(lineString)
				f.write('\n')

			f.close()

	elif fileType=='pickle':
		fileNameTime = '%s_%i_%i_%i_%i%i%i.pickle' % (outputName,now.day,now.month,now.year,now.hour,now.minute,now.second)
		f = open(fileNameTime,'w')
		print ('Writing energies to file %s...' % fileNameTime)
		pickle.dump(energyOutputResidues,f)

def getResidueResponseTimes(referenceName,perturbedName):
	"""

	"""

	refEnergies = pandas.read_table(referenceName,skiprows=2)
	perEnergies = pandas.read_table(perturbedName,skiprows=2)

	numEnergies = len(refEnergies.columns)-1

	diffEnergies = list()
	
	for i in range(0,numEnergies):
		columnName = refEnergies.columns[i+1]
		diff = refEnergies[columnName] - perEnergies[columnName]

		diffEnergies.append(diff.values)


	for i in range(0,numEnergies):
		for k in range(0,len(diffEnergies[i])):
			if abs(diffEnergies[i][k]<0.01):
				diffEnergies[i][k] = 0

	residueResponseTimes = list()
	for diffResidue in diffEnergies:

		# For residues with no response, assign a value of length of trajectory.
		# For residues with response, assign the first frame with a non-zero element.
		if int(np.sum(diffResidue)) != 0:
			responseTime = np.nonzero(diffResidue)[0][0]
			residueResponseTimes.append(responseTime)
		else:
			residueResponseTimes.append(0)

	residueResponseTimes = np.asarray(residueResponseTimes)
	
	return residueResponseTimes

def getDirectedGraph(pdbFile,responseTimes,sourceResidue,targetResidue,toPlot=True):
	"""

	"""

	# Get the undirected graph.
	graph = getGraph(pdbFile,cutoff=6)

def getGraph(pdbFile,cutoff):
	"""

	"""

	# Parse the PDB and extract carbon alphas
	system = parsePDB(pdbFile)
	calphas = system.select('protein and calpha')
	numResidues = len(calphas)
	
	# Start a NetworkX directed graph and add nodes from residue indices
	graph = nx.Graph()
	graph.add_nodes_from(calphas.getResindices())

	# Create edge tuples for the network according to the cutoff value given.
	edgeTuples = list()
	for i in range(0,numResidues):
		residueContacts = calphas.select('within %i of resindex %i' % (cutoff,i))
		contactIndices = residueContacts.getResindices()

		for index in contactIndices:
			
			if index != i:
				edgeTuples.append((i,index))

	# Add identified edges.
	graph.add_edges_from(edgeTuples)

	return graph
