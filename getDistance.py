#!/usr/bin/env python

# IMPORTS

from prody import *
import matplotlib.pyplot as plt
import numpy as np
import datetime, argparse

# INPUT PARSING

parser = argparse.ArgumentParser(description='Get the distance between two\
 selected atoms over the course of an MD trajectory')

parser.add_argument('pdb',type=str,nargs=1,help='Name of the corresponding pdb \
 file of the DCD trajectory')

parser.add_argument('dcd',type=str,nargs=1,help='Name of the trajectory DCD \
	file')

parser.add_argument('-sel1',type=str,nargs='+',
	help='A ProDy atom selection string which determines the first atom')

parser.add_argument('-sel2',type=str,nargs='+',
	help='A ProDy atom selection string which determines the second atom')

parser.add_argument('--outfile',type=str,nargs=1,default=['distance.dat'],
	help='If specified, the results will be written into OUTFILE')

parser.add_argument('--toplot',action='store_true',help='When given, this \
	argument will return a plot of distance versus Frames')

args = parser.parse_args()

# METHODS

def getDistance(traj_tuple,sel1,sel2,toPlot,outFile):

	# Load the trajectory and system as usual.
	activeTraj = Trajectory(traj_tuple[1])
	activeSystem = parsePDB(traj_tuple[0])
	activeTraj.link(activeSystem)
	# Set reference coordinates to frame zero!!!
	activeTraj.setCoords(activeTraj.getFrame(0)) 
	activeTraj.setAtoms(activeSystem.select('name CA'))

	numFrames = activeTraj.numFrames()

	distance = np.zeros(numFrames)

	# Calculate the distance
	for i in range(0,numFrames):
		activeFrame = activeTraj.getFrame(i)
		activeFrame.superpose()
		activeAtoms = activeFrame.getAtoms()
		distance[i] = calcDistance(activeAtoms.select(sel1),
			activeAtoms.select(sel2))

	# Write the results into outFile
	now = datetime.datetime.now()
	f = open(outFile,'w')
	f.write('# getDistance.py was run on: \n')
	f.write('# \n')
	f.write('# Date: %d.%d.%d %d:%d \n' % (now.year,now.month,now.day,now.hour,
		now.minute))
	f.write('# \n')
	f.write('# with the following options:\n')
	f.write('# \n')
	f.write('# --pdb\t\t\t%s\n' % traj_tuple[0])
	f.write('# --dcd\t\t\t%s\n' % traj_tuple[1])
	f.write('# --sel1\t\t\t%s\n' % sel1)
	f.write('# --sel2\t\t\t%s\n' % sel2)
	f.write('# --outfile\t\t\t%s\n' % outFile)
	f.write('# \n')
	f.write('Frame\tDistance\n')

	for i in range(0,numFrames):
		f.write('%g\t%g\n' % (i,distance[i]))

	f.close()

	# Plot, if requested
	if toPlot == True:

		plt.plot(distance)
		plt.show()

# CALLS

if len(args.sel1) > 1:
	sel1 = ' '.join(args.sel1)
else:
	sel1 = args.sel1[0]

if len(args.sel2) > 1:
	sel2 = ' '.join(args.sel2)
else:
	sel2 = args.sel2[0]

getDistance([args.pdb[0],args.dcd[0]],sel1=sel1,sel2=sel2,toPlot=args.toplot,
	outFile=args.outfile[0])

