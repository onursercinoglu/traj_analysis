import re
import numpy as np
import copy
from prody import *
import os
import csv
import time
import datetime
from subprocess import call
import glob
import multiprocessing
import matplotlib.pyplot as plt
import pickle
from operator import itemgetter

def batchPqr(inputFolder=os.getcwd(),outputFolder=os.getcwd(),options='--chain --hbond --salt --ff=CHARMM',pdb2pqrAlias='pdb2pqr',noCores=multiprocessing.cpu_count()):
# Performs pdb2pqr on every pdb file in a given folder. pdb2pqr in turn writes .pqr and .hbond files.

    # Obtain the names of all PDB files in the inputFolder.
    pdbFileNames = glob.glob('*.pdb')

    if noCores==1:

        batchPqrSingleCore(pdbFileNames,inputFolder,outputFolder,options,pdb2pqrAlias)

    elif noCores>1:

        # Prepare for parallel processing using multiprocessing module.
        pool = multiprocessing.Pool(noCores)

        # Split this list into sub-lists for input to each processor.
        pdbFileNameChunks = np.array_split(pdbFileNames,noCores)

        # Call batchPqrSingleCore on multiple cores
        for pdbFileNameChunk in pdbFileNameChunks:
            pool.apply_async(batchPqrSingleCore,args=(pdbFileNameChunk,inputFolder,outputFolder,options,pdb2pqrAlias))

        pool.close()
        pool.join()                                             

def batchPqrSingleCore(pdbFileNameChunk,inputFolder,outputFolder,options,pdb2pqrAlias):
# NOT to be called directly!!!
# Performs pdb2pqr on every pdb file in a given folder. pdb2pqr in turn writes .pqr and .hbond files.

    # First, get the number of PDB files to be analysed.
    numPDBfiles = len(pdbFileNameChunk)

    # Start an output counter.
    pqrOutputCounter = 0

    # Run pdb2pqr for all PDB files that are scanned.
    for pdbFileName in pdbFileNameChunk:
        print ('%s percent completed' % str(float(pqrOutputCounter)*float(100)/float(numPDBfiles)))
        #callArgs = [probeAlias,('-%s' % 'self'),selection,pdbFileName,'>>',('%s.kin' % pdbFileName)]
        callString = ('%s %s %s %s.pqr' % (pdb2pqrAlias,options,pdbFileName,pdbFileName))
        #print callString
        call(callString,shell=True)

        pqrOutputCounter = pqrOutputCounter+1

def batchPqr2Pdb(inputFolder=os.getcwd(),outputFolder=os.getcwd()):
# Converts all pqr files in a given folder into pdb files.

    pqrFileNames = glob.glob('*.pqr')
    numPQRfiles = len(pqrFileNames)
    pdbOutputCounter = 0

    for pqrFileName in pqrFileNames:
        pqrSystem = parsePQR(pqrFileName)
        writePDB(('%s.pdb' % pqrFileName),pqrSystem)

def parsePqrOutput(pqrFileName,regExpPqrOutputFiles,multipleCores=False,matchLevel='Atom',toPickle=False):
    
    # This reads all PROBE Outputs files that match the regExpProbeOutputFiles regular expression by calling readProbeOutput, then merges them in a single list.
    pqrOutputFileNames = glob.glob(regExpPqrOutputFiles)
    pqrOutputFileNames.sort()

    # (!!!DEFECT!!!) If multipleCores options is selected, parse outputs simultaneously using multiple processors.
    if multipleCores:

        availableCores = multiprocessing.cpu_count()
        #availableCores = availableCores-int(2)
        pool = multiprocessing.Pool()

        pqrOutputFileNameChunks = np.array_split(pqrOutputFileNames,availableCores)
        pqrFileNameChunks = [pqrFileName]*availableCores
        inputList = zip(pqrOutputFileNameChunks,qrFileNameChunks)
        
        results = list(pool.map(parsePqrOutputSingleCore,inputList))
        #pqrOutputs = list()
        #return atomContactMatrixSum, atomContactOnOffStates

        #pool.join()
        pool.close()

    # Else, use only a single processor.
    else:

        uniqueContacts = parsePqrOutputSingleCore((pqrOutputFileNames,pqrFileName),matchLevel)

    # If desired to save uniqueContacts to a file, then save it using pickle.
    if toPickle:

        saveUniqueContacts(uniqueContacts)

    return uniqueContacts

    #def mergeResults(results):

def parsePqrOutputSingleCore(inputTuple,matchLevel):
# NOT to be called directly.

    # Extract pqr output file names and get the number of files. 
    pqrOutputFileNames = inputTuple[0]
    numFiles = len(pqrOutputFileNames)

    # Extract pdb file name and parse it using ProDy.
    pqrFileName = inputTuple[1]
    pqrSystem = parsePQR(pqrFileName)

    # Start a list of unique contacts to be filled as the analysis proceeds.
    uniqueContacts = list()

    # For each file name, parse the pqr output and fill uniqueContacts list.
    for i in range(numFiles):
        fileName = pqrOutputFileNames[i]
        uniqueContacts = readPqrOutput(pqrFileName,fileName,i,numFiles,matchLevel,uniqueContacts)
        print 'Total uniqueContacts: %i (%i out of %i files scanned)' % (len(uniqueContacts),i,numFiles)

    # For each unique contact found, get the number of on-off states.
    uniqueContacts = assignNumOnOff(uniqueContacts)

    # Return the list of unique contacts.
    print 'returning uniqueContacts'
    return uniqueContacts
    
def readPqrOutput(pqrFileName,pdb2pqrOutputFileName,numFile,numFiles,matchLevel,uniqueContacts=None):
# This parses .hhond or .salt output files from pdb2pqr in which lists of hydrogen bonds and' salt bridges are found for a PDB.
    
    # First, parse the system using ProDy, so that we know our system in detail.
    pqrSystem = parsePQR(pqrFileName)

    # Open the file for reading, and read all the lines.
    f = open(pdb2pqrOutputFileName,'r')
    data = f.readlines()

    # Start a list to accumulate contacts occuring in THIS frame (pqr)
    pqrOutput = list()

    # For each line (contact), do the following.
    for line in data:
        
        #print 'Parsing contacts in a snapshot...'
        
        # Start a dictionary to save specifics of the contact.
        contact = {'from':[],'to':[],'indexTuple':[],'onOffArray':[],'numOnOff':[]}

        # Parse the line to extract contacting pairs and their details.
        contactMatches = re.search('(Donor|Cation):\s(\D\D\D)\s(\D)\s(\d+)\s(\S+)\t(Acceptor|Anion):\s(\D\D\D)\s(\D)\s(\d+)\s(\S+)',line)
        
        # Sometimes pdb2pqr program generates comment lines, so ignore them by detecting empty contactMatches.
        if not contactMatches:
            continue # Stop execution and continue to the next line in data.

        # Assign the "from" atom and its details. From is 'Donor' in hydrogen bonds and 'Cation' in salt bridges.
        contact['from'] = {'residue':contactMatches.group().split()[1],
        'chain':contactMatches.group().split()[2],
        'resnum':contactMatches.group().split()[3],
        'atomtype':contactMatches.group().split()[4],
        'index':[]}

        # Assign the "to" atom and its details. TO is 'Acceptor' in hydrogen bonds and 'Anion' in salt bridges.
        contact['to'] = {'residue':contactMatches.group().split()[6],
        'chain':contactMatches.group().split()[7],
        'resnum':contactMatches.group().split()[8],
        'atomtype':contactMatches.group().split()[9],
        'index':[]}

        # Find from and to atoms in our system.
        fromInPdb = pqrSystem.select('chain %s resnum %s name %s' % (contact['from']['chain'],contact['from']['resnum'],contact['from']['atomtype']))
        toInPdb = pqrSystem.select('chain %s resnum %s name %s' % (contact['to']['chain'],contact['to']['resnum'],contact['to']['atomtype']))

        # Check the matchLevel. It can be either 'Atom' (default) or 'Residue'.
        # If it is Atom, build index values based on atom indices.
        # If it is Residue, build index values based on residue indices.
        if matchLevel=='Atom':
            
            fromIndex = fromInPdb.getIndices()[0]
            toIndex = toInPdb.getIndices()[0]

        elif matchLevel=='Residue':

            fromIndex = fromInPdb.getResindices()[0]
            toIndex = toInPdb.getResindices()[0]
            contact['from']['atomtype'] = ''
            contact['to']['atomtype'] = ''
        
        contact['from']['index'] = fromIndex
        contact['to']['index'] = toIndex

        # Assign the 'indexTuple' property. This is later used when checking for unique contacts.
        contact['indexTuple'] = (fromIndex,toIndex)

        # Check whether uniqueContacts list is provided, if yes AND if the current contact is already not in the list of pqrOutput (the list of contacts in THIS frame), continue. 
        if type(uniqueContacts)==list and isMatchContacts(contact,pqrOutput)==False:
            
            # Check whether the current contact is already not in the list of uniqueContacts.
            if not isMatchContacts(contact,uniqueContacts):

                # If not, create the onOffArray for it and add it to the list of uniqueContacts.
                contact['onOffArray'] = np.zeros([1,numFiles])
                contact['onOffArray'][0,numFile] = 1
                uniqueContacts.append(contact)

            else:    

                #print 'Updating onOffArray of a previously occured contact...'
                
                # If yes, find the index of matching contact in uniqueContacts.
                matchIndex = next(index for (index,d) in enumerate(uniqueContacts) if d['indexTuple'] == contact['indexTuple'])

                if type(matchIndex)==int:

                    # Append a '1' to its onOffArray to indicate that this contact has been observed in this frame. 
                    uniqueContacts[matchIndex]['onOffArray'][0,numFile] = 1
                
                else:
                    
                    print 'critical error, more than one contact match observed. the program most probably ran improperly.'

        pqrOutput.append(contact)

    return uniqueContacts

def assignNumOnOff(uniqueContacts):

    numUniqueContacts = len(uniqueContacts)

    for i in range(numUniqueContacts):

        print 'Assigning the number of on & off states for a unique contact (%i/%i)' % (i,numUniqueContacts)
        numOnOff = 0
        onOffArray = uniqueContacts[i]['onOffArray'][0]
        prevOnOff = onOffArray[0]

        for j in range(1,len(onOffArray)):
            currentOnOff = onOffArray[j]
            if currentOnOff != prevOnOff:
                numOnOff = numOnOff + 1
            prevOnOff = currentOnOff

        uniqueContacts[i]['numOnOff'] = numOnOff

    return uniqueContacts

def parseUniqueContacts(uniqueContacts):

    # Get the number of unique contacts and frame number.
    numUniqueContacts = len(uniqueContacts)
    numFrames = len(uniqueContacts[0]['onOffArray'][0])

    # Start the contact occupancy matrix. 
    contOccMat = np.zeros([numUniqueContacts,numUniqueContacts])

    # Start the chi-square matrix.
    phichiSqMat = np.zeros([numUniqueContacts*numUniqueContacts,4])

    # Fill the contact occupancy matrix and chi-square matrix.
    counter = 0
    for i in range(numUniqueContacts):

        print 'Parsign a unique contact now... %i/%i' % (i,numUniqueContacts)
        ionOffArray = uniqueContacts[i]['onOffArray'][0]
        inumOnOff = uniqueContacts[i]['numOnOff']

        for j in range(numUniqueContacts):

            jonOffArray = uniqueContacts[j]['onOffArray'][0]
            jnumOnOff = uniqueContacts[j]['numOnOff']

            N = np.min([inumOnOff,jnumOnOff])

            sumOnOff = ionOffArray + jonOffArray
            diffOnOff = ionOffArray - jonOffArray

            nNonZero = len(np.where(sumOnOff>0)[0])
            n11 = len(np.where(sumOnOff==2)[0])
            n10 = len(np.where(diffOnOff==1)[0])
            n01 = len(np.where(-diffOnOff==1)[0])
            n00 = numFrames - nNonZero

            if i==j:
                
                contOccMat[i,j] = nNonZero

            else:

                contOccMat[i,j] = n11

                # Calculate phi.
                phi = float(n00*n11 - n10*n01)/np.sqrt(float((n00+n10)*(n00+n01)*(n11+n10)*(n11+n01)))

                # Calculate chi-square.
                chiSquare = phi*phi*N

                # Fill chi-square matrix.
                phichiSqMat[counter,:] = [int(i),int(j),phi,chiSquare]

                counter = counter + 1

    # Filter out significant correlations from chiSquareMatrix.
    sigContacts = phichiSqMat[np.where(phichiSqMat[:,3]>3.86)]

    return contOccMat, phichiSqMat, sigContacts

def filterUniqueContacts(uniqueContacts,chain,toPlot=False,toTxt=True,toPickle=True):
# Filters out contacts from or to a specified chain in the input uniqueContacts list that is provided.
# Arguments
# uniqueContacts is the output of parsePqrOutput
# chain specifies the chain for which contacts should be analyzed.
# toPlot specifies whether a simple plot shall be shown after the program is run.
# toFile specifies whether the filtered unique contacts shall be saved in a .txt file.

    # Get the number of unique contacts.
    numUniqueContacts = len(uniqueContacts)

    filteredUniqueContacts = list()

    for i in range(0,numUniqueContacts):

        if (uniqueContacts[i]['from']['chain'] == chain) | (uniqueContacts[i]['to']['chain'] == chain):

            print 'match found, adding to filtered list...'
            filteredUniqueContacts.append(uniqueContacts[i])

    numFilteredUniqueContacts = len(filteredUniqueContacts)

    bar_xlabels = list()
    bar_y = list()
    for i in range(0,numFilteredUniqueContacts):

        numOn = sum(sum(filteredUniqueContacts[i]['onOffArray']))

        fromChain = filteredUniqueContacts[i]['from']['chain']
        fromResnum = filteredUniqueContacts[i]['from']['resnum']
        fromRes = filteredUniqueContacts[i]['from']['residue']
        fromAtom = filteredUniqueContacts[i]['from']['atomtype']

        fromString = ' '.join([fromChain,fromResnum,fromRes])

        toChain = filteredUniqueContacts[i]['to']['chain']
        toResnum = filteredUniqueContacts[i]['to']['resnum']
        toRes = filteredUniqueContacts[i]['to']['residue']
        toAtom = filteredUniqueContacts[i]['to']['atomtype']

        toString = ' '.join([toChain,toResnum,toRes])

        fromToString = ' > '.join([fromString,toString])

        bar_xlabels.append(fromToString)
        bar_y.append(numOn)

        print 'contact %i (from %s %s %s %s to %s %s %s %s) occured %i times.' % (i,fromChain,fromResnum,fromRes,fromAtom,
            toChain,toResnum,toRes,toAtom,numOn)

    if toPlot==True:

        bar_x = np.arange(numFilteredUniqueContacts)
        fig, ax = plt.subplots()
        bar_width = 0.35
        ax.bar(bar_x,bar_y,bar_width)
        ax.set_title('my title')
        ax.set_xticks(bar_x+bar_width)
        ax.set_xticklabels(bar_xlabels,rotation='vertical')
        plt.show()

    if toTxt==True:

        fileName = 'filteredUniqueContacts_chain_%s' % chain
        saveUniqueContacts(filteredUniqueContacts,fileName=fileName,fileType='txt')

    if toPickle==True:

        fileName = 'filteredUniqueContacts_chain_%s' % chain
        saveUniqueContacts(filteredUniqueContacts,fileName=fileName,fileType='pickle')

    return filteredUniqueContacts

def isMatchContacts(contact,contactList,matchLevel='index'):
# Returns True if a the given contact matches a contact in the given contactList, returns False otherwise.

    #Check matchLevel and find matching contacts accordingly.
    if matchLevel == 'index':

        if not any(c['indexTuple'] == contact['indexTuple'] for c in contactList):
        
            return False

        else:
        
            return True

    elif matchLevel == 'residue':

        contact['from']['index'] = '';
        contact['to']['index'] = '';

        #Form a dict with 'from' and 'to' keys. They take the value of the contact's.
        fromToContact = {'from': contact['from'], 'to': contact['to']}
        #fromToContact['from']['index'] = ''; #Remove atom indices. IMPORTANT!
        #fromToContact['to']['index'] = ''; #Remove atom indices. IMPORTANT!

        #Reset indexTuples of contacts in contactList and accumulate them in a list.
        fromTosC = list()

        for c in contactList:

            c['from']['index'] = '';
            c['to']['index'] = '';
            #Form a dict with 'from' and 'to' keys. They take the value of the c's.
            fromToC = {'from': c['from'], 'to': c['to']}
            #fromToC['from']['index'] = ''; #Remove atom indices. IMPORTANT!
            #fromToC['to']['index'] = ''; #Remove atom indices. IMPORTANT!

            fromTosC.append(fromToC)

        #Find whether the given contact matches a contact in the contactList.
        if not any (c == fromToContact for c in fromTosC):

            return False

        else:

            return True

def saveUniqueContacts(uniqueContacts,fileName='uniqueContacts',fileType='pickle',addTimeToFileName=False):
    """
    Saves uniqueContacts to a file.

    Parameters
    ----------
    uniqueContacts: list
        a list of unique contacts (hydrogen bonds), compiled using readPqrOutput. 
        This is typically the output of readPqrOutput.
    fileName: string, optional
        a string, indicating the desired file name of the output (without extension)
        If not specified, default name of 'uniqueContacts' is used.
    fileType: 'pickle' (default) or 'txt', optional
        a string, specifying the extension of the file name.
        If 'pickle', then uniqueContacts list is saved into a pickle. This is usually most useful if further analysis is
        planned which will require loading the uniqueContacts into python environment.
        If 'txt', then a text summary of uniqueContacts is saved into a text file. This is usually most useful if no
        further analysis is planned with the uniqueContacts and only plotting or simple calculations will be performed in a ,
        say, spreadsheeting program like Microsoft Excel.

    Returns
    -------
    none.

    Author
    ------
    Onur Sercinoglu
    """
    if fileType == 'pickle':

        now = datetime.datetime.now()

        if addTimeToFileName==True:
            fileNameTime = ('%s_%i_%i_%i_%i%i%i.pickle' % (fileName,now.day,now.month,now.year,now.hour,now.minute,now.second))
        else:
            fileNameTime = '%s.pickle' % fileName

        file = open(fileNameTime,'w')
        print 'Saving uniqueContacts to file %s now...' % fileNameTime
        pickle.dump(uniqueContacts,file)
        file.close()
        print 'Done.'

    elif fileType == 'txt':

        now = datetime.datetime.now()

        if addTimeToFileName==True:
            fileNameTime = ('%s_%i_%i_%i_%i%i%i.txt' % (fileName,now.day,now.month,now.year,now.hour,now.minute,now.second))
        else:
            fileNameTime = '%s.txt' % fileName

        print ('Saving uniqueContacts to file %s now...' % fileNameTime)
        f = open(fileNameTime,'w')
        f.write(fileNameTime)
        f.write('\n')

        f.write('%s\t\t\t\t%s\t\t\t\t%s\t%s' % ('From','To','Occurence','Occurence (%)'))
        f.write('\n')

        numUniqueContacts = len(uniqueContacts)
        numFrames = len(uniqueContacts[0]['onOffArray'][0])

        for i in range(0,numUniqueContacts):

            numOn = sum(sum(uniqueContacts[i]['onOffArray']))
            numOnPercentage = float(numOn)/float(numFrames)*100

            fromChain = uniqueContacts[i]['from']['chain']
            fromResnum = uniqueContacts[i]['from']['resnum']
            fromRes = uniqueContacts[i]['from']['residue']
            fromAtom = uniqueContacts[i]['from']['atomtype']

            toChain = uniqueContacts[i]['to']['chain']
            toResnum = uniqueContacts[i]['to']['resnum']
            toRes = uniqueContacts[i]['to']['residue']
            toAtom = uniqueContacts[i]['to']['atomtype']
            
            f.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s' % (fromChain,fromResnum,fromRes,fromAtom,
                toChain,toResnum,toRes,toAtom,numOn,numOnPercentage))
            f.write('\n')

        f.close()

        print 'Done.'

def saveContactOverlap(contactPickleArray,matchLevel='index',fileName='overlapContacts',toPercentage=True):
    """
    Saves overlapping contacts of multiple uniqueContacts lists to a .txt file.

    Parameters
    ----------
    contactPickleArray: array_like
        an array of pickle file names (strings) for which the overlapping contacts will be saved.
    matchLevel: 'index' (default) or 'residue', optional
        determines the level of matching.
        If 'index', the contact matches will be searched for on the basis of atom indices. This is usually only useful
        when comparing duplicate simulations.
        If 'residue', the contact matches will be searched for on the basis of chain IDs, atomtypes, residue names
        and residue numbers. This will match contacts which are identical in terms of these properties.
        This option is useful when comparing simulations with different protein sizes but in which same chain IDs, residue 
        names and residue numbers point to the same atoms.
    fileName: 'overlapContacts' (default), optional
        indicates the file name to be saved. 
    toPercentage: boolean, True (default), optional
        indicates whether occupancy values should be converted to percentages.

    Returns
    -------
    out: a list of matching contacts.

    Author
    ------
    Onur Sercinoglu
    """
    
    # Load all uniqueContacts lists to a contactLists list.
    numContactLists = len(contactPickleArray)

    contactLists = list()

    for fileNameContact in contactPickleArray:

        activeFile = open(fileNameContact,'r')
        activeUniqueContacts = pickle.load(activeFile)
        contactLists.append(activeUniqueContacts) # It is import to give a copy here as input (by listing it) as it is modified later on by isMatchContacts

    # Create a list for storing indexTuples, from and to atoms and occurence values for each contactList.
    contactListMatch = list()

    # Find matching contact indices, then save the output in an appropriate form to txt. 
    for i in range(0,numContactLists):

        contactList = contactLists[i]
        
        for contact in contactList:
        
            numFrames = len(contact['onOffArray'][0])
            numOn = sum(sum(contact['onOffArray']))

            if toPercentage==True:
                numOn = float(numOn)/float(numFrames)*100

            if not isMatchContacts(contact,contactListMatch,matchLevel=matchLevel):

                matchingContact = {'from':[],'to':[],'indexTuple':[],'occurence':['']*numContactLists}
                matchingContact['from'] = contact['from']
                matchingContact['to'] = contact['to']

                if matchLevel == 'index':
                    matchingContact['indexTuple'] = contact['indexTuple']
                
                elif matchLevel == 'residue':
                    matchingContact['indexTuple'] = '';

                matchingContact['occurence'][i] = numOn
                
                contactListMatch.append(matchingContact)

            else:

                if matchLevel == 'index':
                    matchIndex = next(index for (index,d) in enumerate(contactListMatch) if d['indexTuple'] == contact['indexTuple'])

                elif matchLevel == 'residue':
                    matchIndex = next(index for (index,d) in enumerate(contactListMatch) if (d['from'] == contact['from'] and d['to'] == contact['to']))

                if type(matchIndex)==int:

                    # Append a '1' to its onOffArray to indicate that this contact has been observed in this frame. 
                    contactListMatch[matchIndex]['occurence'][i] = numOn
                
                else:
                    
                    print 'critical error, more than one contact match observed. the program most probably ran improperly.'

    # Write the contactListMatch into txt file with appropriate tab-delimited formatting.
    now = datetime.datetime.now()
    fileNameTime = ('%s_%i_%i_%i_%i%i%i.txt' % (fileName,now.day,now.month,now.year,now.hour,now.minute,now.second))
    print ('Writing the results to file %s...' % fileNameTime)
    f = open(fileNameTime,'w')
    f.write(fileNameTime)
    f.write('\n')

    f.write('This file lists overlapping contacts between the following trajectories:')
    f.write('\n')

    for contactPickle in contactPickleArray:
        f.write(contactPickle)
        f.write('\n')

    f.write('\n')

    if toPercentage == True:
        f.write('\t\t\t\t\t\t\t\t\t\t\t%s' % 'Occurence (%)')
    else:
        f.write('\t\t\t\t\t\t\t\t\t\t\t%s' % 'Occurence')
        
    f.write('\n')

    f.write('%s\t%s\t\t\t%s\t\t\t\t\t' % ('indexTuple','From','To'))

    for i in range(0,numContactLists):
        f.write('%s' % contactPickleArray[i])
        f.write('\t')

    f.write('\n')

    for contact in contactListMatch:

        indexTuple = contact['indexTuple']

        fromChain = contact['from']['chain']
        fromResnum = contact['from']['resnum']
        fromRes = contact['from']['residue']
        fromAtom = contact['from']['atomtype']

        toChain = contact['to']['chain']
        toResnum = contact['to']['resnum']
        toRes = contact['to']['residue']
        toAtom = contact['to']['atomtype']
        
        f.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t' % (indexTuple,fromChain,fromResnum,fromRes,fromAtom,
            toChain,toResnum,toRes,toAtom))

        for i in range(0,numContactLists):
            f.write('%s' % contact['occurence'][i])
            f.write('\t')

        f.write('\n')

    f.close()

    print 'Done.'

    return contactListMatch
#def showContactOverlap(arrayContacts):

def atom2res(uniqueContacts,toFile=True,fileType='pickle'):
# Converts an atom-to-atom uniqueContacts output (default output) into a residue-to-residue format.
    
    # Get the number of unique contacts.
    numContacts = len(uniqueContacts)

    # Start a list to accumulate residue level unique contacts.
    uniqueContactsAtom2Res = list()

    # Start a list to store unique contact chain-residue-resnum keys.
    uniqueContactKeys = list()

    # Accumulate contacts based on residue-residue matches (undirectional).
    for i in range(0,numContacts):
        fromChain = uniqueContacts[i]['from']['chain']
        fromResidue = uniqueContacts[i]['from']['residue']
        fromResnum = uniqueContacts[i]['from']['resnum']

        toChain = uniqueContacts[i]['to']['chain']
        toResidue = uniqueContacts[i]['to']['residue']
        toResnum = uniqueContacts[i]['to']['resnum']

        # Generate to from-to key strings.
        keyFrom = '%s%s%s' % (fromChain,fromResidue,fromResnum)
        keyTo = '%s%s%s' % (toChain,toResidue,toResnum)

        # Sort for direction-blind check, e.g. when from and to residues are in opposite places but we do not want to discriminate between them.
        sortedKeys = sorted([keyFrom,keyTo])

        keyFound = False
        for k in range(0,len(uniqueContactKeys)):
            
            if sortedKeys == uniqueContactKeys[k]:
                uniqueContactsAtom2Res[k].append(uniqueContacts[i])
                keyFound = True

        if keyFound == False:
            uniqueContactKeys.append(sortedKeys)
            uniqueContactsAtom2Res.append([uniqueContacts[i]])

    uniqueContactsResidue = list()

    # Calculate hbond arrays for the new contact format and reshape the output into a nicer format.
    for i in range(0,len(uniqueContactsAtom2Res)):
        res2resContacts = uniqueContactsAtom2Res[i]

        onOffOverlap = np.zeros(len(res2resContacts[0]['onOffArray']))

        for k in range(0,len(res2resContacts)):
            onOffOverlap = onOffOverlap + res2resContacts[k]['onOffArray']

        onOffOverlap[onOffOverlap >= 1] = 1

        # Start a dictionary to save specifics of the contact.
        contact = {'from':[],'to':[],'indexTuple':[],'onOffArray':[],'numOnOff':[]}

        # Assign the "from" atom and its details. From is 'Donor' in hydrogen bonds and 'Cation' in salt bridges.
        contact['from'] = {'residue':res2resContacts[0]['from']['residue'],
        'chain':res2resContacts[0]['from']['chain'],
        'resnum':res2resContacts[0]['from']['resnum'],
        'atomtype':'',
        'index':'%s%s' % (res2resContacts[0]['from']['chain'],res2resContacts[0]['from']['resnum'])}

        # Assign the "to" atom and its details. From is 'Donor' in hydrogen bonds and 'Cation' in salt bridges.
        contact['to'] = {'residue':res2resContacts[0]['to']['residue'],
        'chain':res2resContacts[0]['to']['chain'],
        'resnum':res2resContacts[0]['to']['resnum'],
        'atomtype':'',
        'index':'%s%s' % (res2resContacts[0]['to']['chain'],res2resContacts[0]['to']['resnum'])}

        contact['indexTuple'] = (contact['from']['index'],contact['to']['index'])

        # Save the new onOffOverlap.
        contact['onOffArray'] = onOffOverlap

        # Append it to uniqueContactsResidue
        uniqueContactsResidue.append(contact)

    # Assign number of on-offs
    uniqueContactsResidue = assignNumOnOff(uniqueContactsResidue)

    return uniqueContactsResidue, uniqueContactsAtom2Res, uniqueContactKeys

def sortUniqueContacts(uniqueContacts,switchSortChains=False,sortOrder=['chain','resnum','residue'],reverse=False):
# Sorts uniqueContacts according to a determined order.

    numContacts = len(uniqueContacts)

    # Create an intermediate-list to make sorting on it.
    sortList = list()
    for i in range(0,numContacts):
        contactId = i
        fromChain = uniqueContacts[i]['from']['chain']
        fromResnum = uniqueContacts[i]['from']['resnum']
        fromResidue = uniqueContacts[i]['from']['residue']
        toChain = uniqueContacts[i]['to']['chain']
        toResnum = uniqueContacts[i]['to']['resnum']
        toResidue = uniqueContacts[i]['to']['residue']

        fromToChains = [fromChain,toChain]
        
        if switchSortChains == True:
            fromToChainsSorted = sorted(fromToChains)

            if fromToChainsSorted != fromToChains:
                fromContact = uniqueContacts[i]['from']
                toContact = uniqueContacts[i]['to']
                uniqueContacts[i]['from'] = toContact
                uniqueContacts[i]['to'] = fromContact

            fromToChains = fromToChainsSorted

        sortList.append([contactId,fromToChains[0],int(fromResnum),fromResidue,fromToChains[1],int(toResnum),toResidue])

    # Do the actual sorting.
    sortColumns = list()
    for i in range(0,len(sortOrder)):
        sortKey = sortOrder[i]

        if sortKey == 'chain':
            sortColumn = 1
        elif sortKey == 'resnum':
            sortColumn = 2
        elif sortKey == 'residue':
            sortColumn = 3

        sortColumns.append(sortColumn)

    if len(sortColumns) == 1:
        getter = itemgetter(sortColumns[0])
    elif len(sortColumns) == 2:
        getter = itemgetter(sortColumns[0],sortColumns[1])
    elif len(sortColumns) == 3:
        getter = itemgetter(sortColumns[0],sortColumns[1],sortColumns[2])

    sortList = sorted(sortList,key=getter)

    # Sort uniqueContacts
    uniqueContactsSorted = list()

    for i in range(0,numContacts):
        uniqueContactsSorted.append(uniqueContacts[sortList[i][0]])

    return uniqueContactsSorted