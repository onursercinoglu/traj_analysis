set a {}
set sel [atomselect top "name CA"]
for {set i 0} {$i <= 10000} {incr i} {
	$sel frame $i
	lappend a [measure rgyr $sel]
}
$sel delete