function correlation= new_CrossCorr_calculation(varargin)

%% Modified on: 04.07.2014 by Onur Serçinoğlu
%% Parse the input parameters.
p = inputParser;
p.addRequired('dcdname',@(x)ischar(x));
p.addRequired('res2read',@(x)isnumeric(x));

p.parse(varargin{:});

dcd_filename = p.Results.dcdname;
res2read = p.Results.res2read;

%% Read the dcd.
dcd = readdcd(dcd_filename,res2read);

%% Do the cross-correlation calculation.
frameCount =size(dcd,1);
alphaCount = size(dcd,2)/3;
r = zeros(alphaCount, 3, frameCount);
for i=1:frameCount
    for j=1:alphaCount
        r(j,:,i)= [dcd(i,(3*j-2)) dcd(i,(3*j-1)) dcd(i,(3*j))];
    end
end
r_mean = mean(r, 3);

unnorm_correlation = zeros(alphaCount , alphaCount );
for f = 1:frameCount     
    unnorm_correlation  = unnorm_correlation+ (r(:, :, f) - r_mean) * (r(:, :, f) - r_mean)' / (frameCount );
end
correlation = unnorm_correlation ./ sqrt(diag(unnorm_correlation) * diag(unnorm_correlation)');

%% Generate figures.
figure;
contourf(correlation,'LineStyle','none');
xlabel('Residue number','FontSize',15);ylabel('Residue number','FontSize',15);

filename_suffix = [num2str(res2read(1)) '_' num2str(res2read(end))];
filename = [dcd_filename '_' filename_suffix];
title([filename ' Cross Correlations'],'FontSize',12,'interpreter','none')
axis square;colorbar;caxis([-1 1]);
set(gca,'XTick',(0:20:alphaCount));
set(gca,'YTick',(0:20:alphaCount));
set(gcf, 'Color', 'w')

%% Save plots to .fig files.

saveas(gcf,[filename '_new_Cross_Corr.fig']);
save([filename '_new_Cross_Corr.mat'],'r', 'correlation','frameCount', 'alphaCount')