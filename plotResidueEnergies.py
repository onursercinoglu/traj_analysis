#!/usr/bin/env python

# IMPORTS
import argparse
import pickle
import matplotlib.pyplot as plt
import numpy as np
import pandas
from prody import *

# INPUT PARSING
# INPUT PARSING
parser = argparse.ArgumentParser(description='')

parser.add_argument('pdb',type=str,nargs=1)

parser.add_argument('pickle',type=str,nargs=1,default=[False]) 

parser.add_argument('sourceres',type=int,nargs=1,help='sourceres')

parser.add_argument('--framerange',type=int,default=[False],nargs='+',
	help='If specified, then only FRAMERANGE section of the energies pickle \
	will be handled')

parser.add_argument('--vminmax',type=int,default=[50],nargs=1,
	help='Lower and upper limits of the heatmap')

parser.add_argument('--targetsel',type=str,default=['all'],nargs='+')

# parser.add_argument('targetres',type=int,nargs=1,help='targetres') 

args = parser.parse_args()


# METHODS
def plotResidueEnergies(pdbFile,pickleFile,sourceResid,frameRange=False,energyMinMax=[50],targetSel='all'):
	
	energyMinMax = [(energyMinMax - 2*energyMinMax),energyMinMax]

	energies = pickle.load(open(pickleFile,'r'))
	numFrames = len(energies['Total'][0][0].values)
	
	system = parsePDB(pdbFile)
	targetSelection = system.select(targetSel)
	targetResids = np.unique(targetSelection.getResindices())
	numTargetResids = len(targetResids)

	if frameRange == False:
		frameRange = [0,numFrames]
	
	heatMapEnergies = np.zeros([numTargetResids,frameRange[1]-frameRange[0]])

	numHeatMapFrames = heatMapEnergies.shape
	numHeatMapFrames = numHeatMapFrames[1]

	for i in range(0,len(targetResids)):
		heatMapEnergies[i,:] = energies['Total'][sourceResid][targetResids[i]].values[frameRange[0]:frameRange[1]]

	fig, ax = plt.subplots()
	heatmap = ax.pcolor(heatMapEnergies,cmap=plt.cm.Spectral, alpha=0.8, vmin=energyMinMax[0], vmax=energyMinMax[1])
	ax.set_frame_on(False)
	ax.grid(False)
	#ax.set_yticks(map(int,np.linspace(0,numResidues,11)))
	#ax.set_yticklabels(map(int,np.linspace(0,numResidues,11)))
	ax.set_ylim([0,numTargetResids-1])
	ax.set_ylabel('Residues')
	ax.set_xticks(map(int,np.linspace(0,numHeatMapFrames,11)))
	ax.set_xticklabels(map(int,np.linspace(frameRange[0],frameRange[1],11)))
	ax.set_xlabel('Frames')
	plt.colorbar(heatmap)
	fig = plt.gcf()
	plt.show()

# CALLS

pdbFile = args.pdb[0]

pickleFile = args.pickle[0]

sourceResid = args.sourceres[0]

if len(args.framerange) > 1:
	frameRange = np.asarray(args.framerange)
else:
	frameRange = args.framerange[0]

if len(args.targetsel) > 1:
	targetSel = ' '.join(args.targetsel)
else:
	targetSel = args.targetsel[0]


vminmax = args.vminmax[0]

plotResidueEnergies(pdbFile=pdbFile,pickleFile=pickleFile,sourceResid=sourceResid,frameRange=frameRange,energyMinMax=vminmax,
	targetSel= targetSel)