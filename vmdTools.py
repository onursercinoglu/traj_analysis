import re
import datetime


def makeResidueHBondPretty(fileName,aaNameStyle='three-letter'):
    """
    Converts a VMD formatted HBonds Plugin output (typically hbonds-details.dat),
    generated using residue contacts option, to a prettier format which can be more easily utilized in spreadsheeting software.

    Parameters
    ----------
    fileName: string
        a string containing the name of the file which is desired to be converted.
    aaNameStyle: string, 'three-letter' (default), 'one-letter' or 'full-name', optional
    	a string specifying the style of amino acid labeling. 
    
    Returns
    -------
    none.

    Author
    ------
    Onur Sercinoglu
    """
	# Open the file.
    f = open(fileName,'r')

    # Open the file for reading, and read all the lines.
    f_lines = f.readlines()

    # Start a list to accumulate contacts occuring in this file.
    hbonds = list()

    # For each line (contact), do the following.
    for line in f_lines[2:]:

        # Start a dictionary to save specifics of the contact.
        contact = {'from':[],'to':[],'occurence':[]}

        # Parse the line to extract contacting pairs and their details.
        contactMatches = re.search('(\D+)(\d+)-(Main|Side)\s+(\D+)(\d+)-(Main|Side)\s+(.+)%',line)

        # Assign the "from" atom and its details.
     	contact['from'] = {'residue':contactMatches.groups()[0],
     	'resnum':contactMatches.groups()[1],
     	'residuePart':contactMatches.groups()[2]}

    	# Assign the "to" atom and its details.
     	contact['to'] = {'residue':contactMatches.groups()[3],
     	'resnum':contactMatches.groups()[4],
     	'residuePart':contactMatches.groups()[5]}

     	# Assign the occurence of the contact.
     	contact['occurence'] = contactMatches.groups()[6]

     	hbonds.append(contact)

    # Now write the results.
	# now = datetime.datetime.now()
    fileNamePretty = ('%s_pretty.txt' % (fileName))
    print ('Saving to file %s now...' % fileNamePretty)
    f = open(fileNamePretty,'w')
    f.write(fileNamePretty)
    f.write('\n')
    f.write('%s\t\t\t%s\t\t\t%s' % ('From','To','Occurence (%)'))
    f.write('\n')

    numHbonds = len(hbonds)

    for i in range(0,numHbonds):

        fromResnum = hbonds[i]['from']['resnum']
        fromRes = hbonds[i]['from']['residue']
        fromPart = hbonds[i]['from']['residuePart']

        toResnum = hbonds[i]['to']['resnum']
        toRes = hbonds[i]['to']['residue']
        toPart = hbonds[i]['to']['residuePart']

        occurence = hbonds[i]['occurence']
        
        f.write('%s\t%s\t%s\t%s\t%s\t%s\t%s' % (fromResnum,fromRes,fromPart,toResnum,toRes,toPart,occurence))
        f.write('\n')

    f.close()

    print 'Done.'
