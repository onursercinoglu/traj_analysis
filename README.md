# README #

**traj_analysis** is a **private repository** and by no means intended for public use. It is created to be a convenient stash of molecular dynamics trajectory analysis tools for my own usage.

**traj_analysis** is a repository where I dump the wrappers I write for my routine analysis tasks on Molecular Dynamics trajectories in DCD format. Most of the packages and files are in Python programming language (2.7), although some MATLAB code and tcl scripts that depend on VMD and NAMD installation are present as well. 

**I definitely would not recommend the use of this repo as a library for your MD trajectory analysis needs. You probably would not like it**. If you're looking for an easy-to-use library for trajectory analysis, there are many options such as ProDy, mdanalysis, mdtraj (Python) and mdtoolbox (MATLAB). However, you might still be interested in the use cases 

## DEPENDENCIES ##

Most scripts are dependent on several Python packages. These can be found at the beginning of each wiki page.

## USAGE ##

Most of the tools with .py extension can be called directly from the terminal: e.g. `./getRMSD.py <options>`.
For a full list of options for each tools, append `--help` to the call.

A comprehensive walkthrough regarding the usage of each tool is on the way!

## AUTHOR ##

Onur Serçinoğlu