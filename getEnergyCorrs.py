#!/usr/bin/env python

# IMPORTS

import os
import pickle
import matplotlib.pyplot as plt
import numpy as np
import pandas
import itertools
import array
from pylab import *
from scipy import stats
import pyprind
from prody import *
import argparse
import datetime
from natsort import natsort

# INPUT PARSING
parser = argparse.ArgumentParser(description='Calculate linear correlations \
	for previously computed pairwise residue interaction energies from a \
	MD trajectory')

parser.add_argument('pdb',type=str,nargs=1,help='Name of the corresponding pdb \
 file of the energies pickle file')

parser.add_argument('pickle',type=str,nargs=1,help='Name of the energies pickle\
	 file')

parser.add_argument('--framerange',type=int,default=[False],nargs='+',
	help='If specified, then only FRAMERANGE section of the energies pickle \
	will be handled')

parser.add_argument('--sourcesel',type=str,nargs='+',
	help='A ProDy atom selection string which determines the first group of \
	selected residues. ')

parser.add_argument('--targetsel',type=str,nargs='+',
	help='A ProDy atom selection string which determines the second group of \
	selected residues.')

parser.add_argument('--r2cutoff',type=float,default=[0.8],nargs=1,
	help='Minimum R-squared cutoff ')

parser.add_argument('--outfile',type=str,nargs=1,default=['energyCorrelations.dat'],
	help='If specified, the results will be written into OUTFILE')

parser.add_argument('--numcores',default=[1],type=int,nargs=1,
	help='(DEFUNCT) Number of CPU cores to be employed for calculation. If not specified, it \
	defaults to 1 (no parallel computation will be done). If specified, e.g. numcores=n, \
	then n cores will be used to distribute the computational workload')

args = parser.parse_args()

# METHODS

def getEnergyCorrs(pdb,pickleFile='',frameRange=False,
	sourceSel='',targetSel='',r2cutoff='',outFile='energyCorrelations.dat',
	numCores=1):

	# Get source residue(s)
	system = parsePDB(pdb)
	source = system.select(sourceSel)
	sourceResids = np.unique(source.getResindices())
	numSourceRes = len(sourceResids)

	# Get target residue(s)
	target = system.select(targetSel)
	targetResids = np.unique(target.getResindices())
	numTargetRes = len(targetResids)

	# Get all residue(s)
	allResidues = system.select('protein')
	allResids = np.unique(allResidues.getResindices())
	numAllRes = len(allResids)

	# Load the pickled pairwise energies.
	energiesFile = open(pickleFile,'r')
	energies = pickle.load(energiesFile)
	energiesFile.close()

	energiesTotal = energies['Total']
	del energies

	# Clean up the energies dict by removing all interactions with all zero values.
	for i in range(0,len(energiesTotal)):
		for j in range(0,len(energiesTotal[i].columns)):
			if (energiesTotal[i][j].values == float(0)).all() == True:
				del energiesTotal[i][j]

	# Iteratively calculate linear regressions btw. all interactions of each
	# residue in sourceSel and all interactions of each residue in targetSel,
	# save correlations with R-square values above the cutoff value, append 
	# them to the high correlations set.
	# Depending on selection sizes, this can take VERY LONG time.
	
	corrList = list()

	progBar = pyprind.ProgBar(numSourceRes)

	for i in range(0,numSourceRes):
		
		sourceRes = sourceResids[i]
		sourcePairs = energiesTotal[sourceRes].columns
		sourceCombs = list(itertools.product([sourceRes],sourcePairs))
		
		for j in range(0,numTargetRes):
		
			targetRes = targetResids[j]
			targetPairs = energiesTotal[targetRes].columns

			#Generate pairwise energy pair combinations for this 
			#source-target pair.
			targetCombs = list(itertools.product([targetRes],targetPairs))
			energyPairs = list(itertools.product(sourceCombs,targetCombs))

			# For each pairwise energy pair combination, compute linear reg.
			for energyPair in energyPairs:

				firstPair = energyPair[0]
				secondPair = energyPair[1]

				# If firstPair and secondPair define same interaction, skip to
				# next iteration.
				if (np.sort(firstPair) == np.sort(secondPair)).all() == True:
					continue
				
				sourceFirstRes = firstPair[0]
				sourceSecondRes = firstPair[1]
				targetFirstRes = secondPair[0]
				targetSecondRes = secondPair[1]
				sourceInteraction = energiesTotal[sourceFirstRes][sourceSecondRes].values
				targetInteraction = energiesTotal[targetFirstRes][targetSecondRes].values
				
				if frameRange[0] == False:
					slope,_,r2,_,_ = stats.linregress(sourceInteraction,targetInteraction)
				else:
					slope,_,r2,_,_ = stats.linregress(sourceInteraction[frameRange[0]:frameRange[1]],
						targetInteraction[frameRange[0]:frameRange[1]])

				# If r-squared value is above r2cutoff, notify.
				if r2 >= r2cutoff:
					#print "correlation btw %i-%i and %i-%i above cutoff value" % (sourceFirstRes,sourceSecondRes,targetFirstRes,targetSecondRes)
					corrList.append([r2,slope,firstPair,secondPair])

		progBar.update()

	# Sort corrList according to decresing r2 values.
	corrList = natsort.natsorted(corrList,reverse=True)

	# Write the results into outfile.
	now = datetime.datetime.now()
	f = open(outFile,'w')
	f.write('# getEnergyCorrs.py was run on: \n')
	f.write('# \n')
	f.write('# Date: %d.%d.%d %d:%d \n' % (now.year,now.month,now.day,now.hour,
		now.minute))
	f.write('# \n')
	f.write('# with the following options:\n')
	f.write('# \n')
	f.write('# --pdb\t\t\t%s\n' % pdb)
	f.write('# --pickle\t\t\t%s\n' % pickleFile)
	f.write('# --framerange\t\t\t%s\n' % str(frameRange))
	f.write('# --sourcesel\t\t\t%s\n' % sourceSel)
	f.write('# --targetsel\t\t\t%s\n' % targetSel)
	f.write('# --r2cutoff\t\t\t%.2f\n' % r2cutoff)
	f.write('# --outfile\t\t\t%s\n' % outFile)
	f.write('# \n')
	f.write('# Following correlations between pairwise energies were found:\n')
	f.write('# \n')
	f.write('R-squared\tPair 1\tPair 2\n')

	for i in range(0,len(corrList)):
		f.write('%.2f\t%i-%i\t%i-%i\n' % (corrList[i][0],
			corrList[i][1][0],corrList[i][1][1],
			corrList[i][2][0],corrList[i][2][1]))
	
	f.close()

# CALLS

if len(args.sourcesel) > 1:
	sourcesel = ' '.join(args.sourcesel)
else:
	sourcesel = args.sourcesel[0]

if len(args.targetsel) > 1:
	targetsel = ' '.join(args.targetsel)
else:
	targetsel = args.targetsel[0]

if len(args.framerange) > 1:
	framerange = np.asarray(args.framerange)
else:
	framerange = args.framerange

getEnergyCorrs(args.pdb[0],pickleFile=args.pickle[0],sourceSel=sourcesel,
	targetSel=targetsel,r2cutoff=args.r2cutoff[0],frameRange=framerange,outFile=args.outfile[0],
	numCores=args.numcores[0])
