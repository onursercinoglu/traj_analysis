import re
from prody import *



class namdConf(object):

	def __init__(self):

		self.description ='A NAMD configuration file'
		self.structure = 'ionized.psf'
		self.coordinates = 'ionized.pdb'
		self.inputName = 'input'
		self.outputName = 'output'
		self.binCoordinates = '$inputname.restart.coor'
		self.binVelocities = '$inputname.restart.vel'
		self.extendedSystem = '$inputname.restart.xsc'
		self.firsttimestep = 0
		self.paraTypeCharmm = 'on'
		self.parameters = 'par_all27_prot_lipid_na.inp'
		self.temperature = 310
		self.wrapWater = 'on'
		self.wrapAll = 'off'
		self.exclude = 'scaled1-4'
		self.oneFourScaling = '1.0'
		self.cutoff = '12.'
		self.switching = 'on'
		self.switchdist = '10.'
		self.pairlistdist = '14.'
		self.margin = '2.0'
		self.timestep = '2.0'
		self.rigidBonds = 'all'
		self.nonbondedFreq = 1
		self.fullElectFrequency = 2
		self.stepspercycle = 10
		self.PME = 'yes'
		self.PMEGridSpacing = 1.0
		self.PMEGridSizeX = ''
		self.PMEGridSizeY = ''
		self.PMEGridSizeZ = ''
		self.langevin = 'on'
		self.langevinDamping = 5
		self.langevinTemp = self.temperature
		self.langevinHydrogen = 'no'
		self.cellBasisVector1 = ''
		self.cellBasisVector2 = ''
		self.cellBasisVector3 = ''
		self.cellOrigin = ''
		self.useGroupPressure = 'yes'
		self.useFlexibleCell = 'no'
		self.useConstantArea = 'no'
		self.langevinPiston = 'on'
		self.langevinPistonTarget = 1.01325
		self.langevinPistonPeriod = '100.'
		self.langevinPistonDecay = '50.'
		self.langevinPistonTemp = '310.'
		self.restartFreq = 500
		self.dcdFreq = 500
		self.xstFreq = 500
		self.outputEnergies = 500
		self.outputPressure = 500
		self.reinitvels = self.temperature
		self.mode = 'min' # can be 'min', 'run' or 'min-run'
		self.setMatches = ''
		self.restart = True # can be True or False, if true, restart files will be used, if false, restart files will not be used.
		self.minimize = 10000
		self.run = 500000

	def readConf(self,confFileName):

		setMatches = dict()
		f = open(confFileName,'r')
		data = f.readlines()
		for line in data:
			line = line.strip()
			if line.startswith('#'):
				continue
			matches = re.search(r'(\S+)\s+(\S+\s*\S*\s*\S*\s*)',line,re.IGNORECASE) # re.search will only keep the first match, and that is good for our purpose.
			if matches:
				parameter = matches.group(1)
				value = matches.group(2)

				if parameter == 'set':
					setVars = re.search(r'(\S+)\s+(\S+)',value) # matching variables declared with 'set'
					if setVars:
						setVarName = setVars.group(1)
						setVarValue = setVars.group(2)
						setMatches[setVarName] = setVarValue
				else:
					setattr(self,parameter,value)

		setattr(self,'setMatches',setMatches)

		f.close()

	def setCellBasisVectorOrigin(self):

		coords = parsePDB(self.coordinates)
		minX = min(coords.getCoords()[:,0])
		minY = min(coords.getCoords()[:,1])
		minZ = min(coords.getCoords()[:,2])
		maxX = max(coords.getCoords()[:,0])
		maxY = max(coords.getCoords()[:,1])
		maxZ = max(coords.getCoords()[:,2])
		meanX = np.mean(coords.getCoords()[:,0])
		meanY = np.mean(coords.getCoords()[:,1])
		meanZ = np.mean(coords.getCoords()[:,2])

		self.cellBasisVector1 = ' '.join([('%.2f' % abs(maxX-minX)),'0','0'])
		self.cellBasisVector2 = ' '.join(['0',('%.2f' % abs(maxY-minY)),'0'])
		self.cellBasisVector3 = ' '.join(['0','0',('%.2f' % abs(maxZ-minZ))])
		self.cellOrigin = ' '.join([('%.2f' % meanX),('%.2f' % meanY),('%.2f' % meanZ)])
		
	def writeConf(self,confFileName):

		f = open(confFileName,'w')
		
		# Set variables
		f.write(' '.join(['#'*40,'\n']))
		f.write('## SET VARIABLES\n')
		f.write(' '.join(['#'*40,'\n']))
		f.write('\n')
		if self.setMatches:
			print 'yes'
			for (key,value) in self.setMatches.iteritems():
				f.write('%-10s\t%s\t\t\t\t%-10s\n' % ('set',key,value))

		# Description block

		f.write(' '.join(['#'*40,'\n']))
		f.write(' '.join(['##',self.description,'\n']))
		f.write(' '.join(['#'*40,'\n']))
		f.write('\n')

		# Adjustable parameters block

		f.write(' '.join(['#'*40,'\n']))
		f.write('## ADJUSTABLE PARAMETERS\n')
		f.write(' '.join(['#'*40,'\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('structure',self.structure))
		f.write('%-10s\t\t\t\t%-10s\n' % ('coordinates',self.coordinates))
		f.write('%-10s\t\t\t\t%-10s\n' % ('outputName',self.outputName))
		f.write('\n')

		if self.restart:
			f.write(' '.join(['## Using restart files:','\n']))
			f.write('\n')
			f.write('%-10s\t\t\t%-10s\n' % ('set inputName',self.inputName))
			f.write('%-10s\t\t\t%-10s\n' % ('binCoordinates','$inputName.restart.coor'))
			f.write('%-10s\t\t\t%-10s\n' % ('binVelocities','$inputName.restart.vel'))
			f.write('%-10s\t\t\t%-10s\n' % ('extendedSystem','$inputName.restart.xsc'))
		else:
			f.write(' '.join(['## Not using restart files:','\n']))
			f.write('\n')
			f.write('%-10s\t\t\t%-10s\n' % ('set temperature',self.temperature))
			f.write('%-10s\t\t\t\t%-10s\n' % ('temperature','$temperature'))

		f.write('\n')
		f.write('%-10s\t\t\t%-10s\n' % ('firsttimestep',self.firsttimestep))
		f.write('\n')

		# Simulation parameters block

		f.write(' '.join(['#'*40,'\n']))
		f.write('## SIMULATION PARAMETERS\n')
		f.write(' '.join(['#'*40,'\n']))
		f.write('\n')
		f.write(' '.join(['## Input','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t%-10s\n' % ('paraTypeCharmm',self.paraTypeCharmm))
		f.write('%-10s\t\t\t\t%-10s\n' % ('parameters',self.parameters))
		f.write('\n')
		f.write(' '.join(['## Force-Field','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('exclude',self.exclude))
		f.write('%-10s\t\t\t\t%-10s\n' % ('1-4scaling',self.oneFourScaling))
		f.write('%-10s\t\t\t\t%-10s\n' % ('cutoff',self.cutoff))
		f.write('%-10s\t\t\t\t%-10s\n' % ('switching',self.switching))
		f.write('%-10s\t\t\t\t%-10s\n' % ('switchdist',self.switchdist))
		f.write('%-10s\t\t\t%-10s\n' % ('pairlistdist',self.pairlistdist))
		f.write('%-10s\t\t\t\t%-10s\n' % ('margin',self.margin))
		f.write('\n')
		f.write(' '.join(['## Integrator','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('timestep',self.timestep))
		f.write('%-10s\t\t\t\t%-10s\n' % ('rigidBonds',self.rigidBonds))
		f.write('%-10s\t\t\t%-10s\n' % ('nonbondedFreq',self.nonbondedFreq))
		f.write('%-10s\t\t%-10s\n' % ('fullElectFrequency',self.fullElectFrequency))
		f.write('%-10s\t\t\t%-10s\n' % ('stepspercycle',self.stepspercycle))
		f.write('\n')
		f.write(' '.join(['## Constant Temperature Control','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('langevin',self.langevin))
		f.write('%-10s\t\t\t%-10s\n' % ('langevinDamping',self.langevinDamping))
		f.write('%-10s\t\t\t%-10s\n' % ('langevinTemp',self.langevinTemp))
		f.write('%-10s\t\t%-10s\n' % ('langevinHydrogen',self.langevinHydrogen))
		f.write('\n')
		# paramValueDict = vars(self)
		# for key, value in paramValueDict.iteritems():
		# 	f.write(('%-10s\t\t\t%10s\n' % (key,value)))

		if self.restart == False: # if restart is False, then we must specify periodic boundary conditions from properties.
			f.write(' '.join(['## Periodic Boundary Conditions','\n']))
			f.write('\n')
			f.write('%-10s\t\t%-10s\n' % ('cellBasisVector1',self.cellBasisVector1))
			f.write('%-10s\t\t%-10s\n' % ('cellBasisVector2',self.cellBasisVector2))
			f.write('%-10s\t\t%-10s\n' % ('cellBasisVector3',self.cellBasisVector3))
			f.write('%-10s\t\t\t\t%-10s\n' % ('cellOrigin',self.cellOrigin))
			f.write('%-10s\t\t\t\t%-10s\n' % ('wrapAll',self.wrapAll))
		else:
			f.write('%-10s\t\t\t\t%-10s\n' % ('wrapWater',self.wrapWater))
			f.write('%-10s\t\t\t\t%-10s\n' % ('wrapAll',self.wrapAll))

		f.write('\n')
		f.write(' '.join(['## PME (for full-system periodic electrostatics)','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('PME',self.PME))
		f.write('%-10s\t\t\t%-10s\n' % ('PMEGridSpacing',self.PMEGridSpacing))

		if float(self.PMEGridSpacing) != 1: # If PMEGridSpacing is not equal to 1.0, specify grid sizes from properties.
			f.write('\n')
			f.write(' '.join(['## Manual grid definitions','\n']))
			f.write('%-10s\t\t\t%-10s\n' % ('PMEGridSizeX',self.wrapAll))
			f.write('%-10s\t\t\t%-10s\n' % ('PMEGridSizeY',self.wrapAll))
			f.write('%-10s\t\t\t%-10s\n' % ('PMEGridSizeZ',self.wrapAll))

		f.write('\n')
		f.write(' '.join(['## Constant Pressure Control (variable volume)','\n']))
		f.write('\n')
		f.write('%-10s\t\t%-10s\n' % ('useGroupPressure',self.useGroupPressure))
		f.write('%-10s\t\t\t%-10s\n' % ('useFlexibleCell',self.useFlexibleCell))
		f.write('%-10s\t\t\t%-10s\n' % ('useConstantArea',self.useConstantArea))
		f.write('%-10s\t\t\t%-10s\n' % ('langevinPiston',self.langevinPiston))
		f.write('%-10s\t%-10s\n' % ('langevinPistonTarget',self.langevinPistonTarget))
		f.write('%-10s\t%-10s\n' % ('langevinPistonPeriod',self.langevinPistonPeriod))
		f.write('%-10s\t\t%-10s\n' % ('langevinPistonDecay',self.langevinPistonDecay))
		f.write('%-10s\t\t%-10s\n' % ('langevinPistonTemp',self.langevinPistonTemp))
		f.write('\n')
		f.write(' '.join(['## Output Frequencies','\n']))
		f.write('\n')
		f.write('%-10s\t\t\t\t%-10s\n' % ('restartFreq',self.restartFreq))
		f.write('%-10s\t\t\t\t%-10s\n' % ('dcdFreq',self.dcdFreq))
		f.write('%-10s\t\t\t\t%-10s\n' % ('xstFreq',self.xstFreq))
		f.write('%-10s\t\t\t%-10s\n' % ('outputEnergies',self.outputEnergies))
		f.write('%-10s\t\t\t%-10s\n' % ('outputPressure',self.outputPressure))
		f.write('\n')

		# Execution
		f.write(' '.join(['#'*40,'\n']))
		f.write('## EXECUTION SCRIPT\n')
		f.write(' '.join(['#'*40,'\n']))
		f.write('\n')

		if self.mode == 'min':
			f.write(' '.join(['## Minimization:','\n']))
			f.write('\n')
			f.write('%-10s\t\t\t\t%-10s\n' % ('minimize',self.minimize))
			f.write('%-10s\t\t\t\t%-10s\n' % ('reinitvels',self.reinitvels))
		elif self.mode == 'run':
			f.write(' '.join(['## MD Run:','\n']))
			f.write('\n')
			f.write('%-10s\t\t\t\t%-10s\n' % ('run',self.run))
		elif self.mode == 'min-run':
			f.write(' '.join(['## Minimization & MD Run:','\n']))
			f.write('\n')
			f.write('%-10s\t\t\t\t%-10s\n' % ('minimize',self.minimize))
			f.write('%-10s\t\t\t\t%-10s\n' % ('reinitvels',self.reinitvels))
			f.write('%-10s\t\t\t\t%-10s\n' % ('run',self.run))

		f.close()

		print ('conf file %s was written with success.' % confFileName)
