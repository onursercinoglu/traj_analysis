#!/usr/bin/env python

# IMPORTS
import argparse
import numpy as np
from natsort import natsort
import re
import copy

# INPUT PARSING

# METHODS
def getEnergyPaths(corrFile,sourceRes,targetRes):

	dataList = list()

	corrFile = open(corrFile,'r')

	lines = corrFile.readlines()

	for line in lines:
		if not (line.startswith('#') or line.startswith('R-squared')):

			# Get rid of newline and tab chars.
			dataline = line.strip('\n').split('\t')

			# Parse source-target pairs properly and sort them.
			sourcePairMatch = re.search('(\d+)-(\d+)',dataline[1])
			sourcePair = sourcePairMatch.groups()
			sourcePair = natsort.natsorted(map(int,sourcePair))
			
			targetPairMatch = re.search('(\d+)-(\d+)',dataline[2])
			targetPair = targetPairMatch.groups()
			targetPair = natsort.natsorted(map(int,targetPair))
			
			sourceTargetList = [sourcePair,targetPair]
			sourceTargetList = natsort.natsorted(sourceTargetList)

			dataline = [float(dataline[0]),sourceTargetList]

			dataList.append(dataline)

	# Uniquify the list
	uniqueData = list()

	for item in dataList:

		if not item in uniqueData:
			uniqueData.append(item)

	totalPathways = list()

	# Find all possible pathway starters for sourceRes
	findPaths(uniqueData,sourceRes,targetRes)

def findPaths(uniqueData,sourceRes,targetRes):

	pathways = list()
	for item in uniqueData:

		# Accumulate potential pathway starters:
		# If the first member of sourcePair includes sourceRes and
		# if the last member of sourcePair and first member of targetPair are the same
		if (item[1][0][0] == sourceRes) and (item[1][0][1] in item[1][1]):
			pathways.append(item)
			bridgeResIndex = item[1][1].index(item[1][0][1])
			bridgeRes = item[1][1][bridgeResIndex]
			itemcopy = copy.deepcopy(item)
			itemcopy[1][1].pop(bridgeResIndex)
			tailRes = itemcopy[1][1][0]

			# If tailRes is equal to targetRes, we found a path!
			if tailRes == targetRes or bridgeRes == targetRes:
				print "found a path!!!!"
				return tailRes
			else:
				findPaths(pathways,tailRes,targetRes)		

	print pathways
	#if pathways:


# CALLS