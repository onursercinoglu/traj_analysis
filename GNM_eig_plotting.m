%clc, clear all, close all

%set(0,'DefaultAxesFontSize',12)

%set(0,'DefaultTextFontSize',12)

%set(0,'DefaultLineLineWidth',1.5)

% %

% dcd = readdcd('pyd_1_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_pyd_1= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('pyd_2_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_pyd_2= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('pyd_3_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_pyd_3= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('pyd_4_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_pyd_4= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('pyd_5_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_pyd_5= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('pyd_6_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_pyd_6= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('mut48_1_200ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_mut48_1= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('mut48_2_200ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_mut48_2= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('mut48_3_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_mut48_3= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('mut48_4_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_mut48_4= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('mut48_5_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_mut48_5= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('mut48_6_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_mut48_6= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('mut41_1_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_mut41_1= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('mut41_2_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_mut41_2= GNM_fdn(dcd, 10);clear dcd

% resnum=size(eig_pyd_1,1);

% nbins=50;

% eigenvalue=87;%for fast modes type higher

% mode=resnum-eigenvalue;

% %GNM higher eigenvalue (beginning of eigenvalue vector) = fast modes, 

% %lowest eigenvalues =slow modes (end of eigenvalue vector)

% % x_beg=1.5;x_end=8;y_end=200;%Slowest Eigenvalue

% x_beg=25;x_end=40;y_end=600;%Fast Eigenvalue

% figure1 = figure('Position',[1000 100 800 800],'Color',[1 1 1]);

% subplot(6,3,1),hist(eig_pyd_1(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% title(['Distribution of Eigenvalues of eigenvector ',num2str(mode)])

% subplot(6,3,4),hist(eig_pyd_2(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% title('Wild Type')

% subplot(6,3,7),hist(eig_pyd_3(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% ylabel('Frequency')

% subplot(6,3,10),hist(eig_pyd_4(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% subplot(6,3,13),hist(eig_pyd_5(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% subplot(6,3,16),hist(eig_pyd_6(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% subplot(6,3,2),hist(eig_mut48_1(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% title('Mutant D48A')

% subplot(6,3,5),hist(eig_mut48_2(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% subplot(6,3,8),hist(eig_mut48_3(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% subplot(6,3,11),hist(eig_mut48_4(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% subplot(6,3,14),hist(eig_mut48_5(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% subplot(6,3,17),hist(eig_mut48_6(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% subplot(6,3,3),hist(eig_mut41_1(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% title('Mutant R41A')

% subplot(6,3,6),hist(eig_mut41_2(mode,:),nbins),axis([x_beg x_end 0 y_end]);

% 

% saveas(figure1,['Eigenvalues_individualSimulations_',num2str(mode),'.fig']);

%% For Multiple Modes

% mode=[87 88 89];

% %GNM higher eigenvalue (beginning of eigenvalue vector) = fast modes, 

% %lowest eigenvalues =slow modes (end of eigenvalue vector)

% x_beg=1.5;x_end=8;y_end=300;%Slowest Eigenvalue

% % x_beg=25;x_end=40;y_end=600;%Fast Eigenvalue

% figure1 = figure('Position',[1000 100 800 800],'Color',[1 1 1]);

% subplot(6,3,1),

% hist([eig_pyd_1(mode(1),:) eig_pyd_1(mode(2),:) eig_pyd_1(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% title(['Distribution of Eigenvalues of eigenvector ',num2str(mode)])

% subplot(6,3,4),

% hist([eig_pyd_2(mode(1),:) eig_pyd_2(mode(2),:) eig_pyd_2(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% title('Wild Type')

% subplot(6,3,7),

% hist([eig_pyd_3(mode(1),:) eig_pyd_3(mode(2),:) eig_pyd_3(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% ylabel('Frequency')

% subplot(6,3,10),

% hist([eig_pyd_4(mode(1),:) eig_pyd_4(mode(2),:) eig_pyd_4(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% subplot(6,3,13),

% hist([eig_pyd_5(mode(1),:) eig_pyd_5(mode(2),:) eig_pyd_5(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% subplot(6,3,16),

% hist([eig_pyd_6(mode(1),:) eig_pyd_6(mode(2),:) eig_pyd_6(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% subplot(6,3,2),

% hist([eig_mut48_1(mode(1),:) eig_mut48_1(mode(2),:) eig_mut48_1(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% title('Mutant D48A')

% subplot(6,3,5),

% hist([eig_mut48_2(mode(1),:) eig_mut48_2(mode(2),:) eig_mut48_2(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% subplot(6,3,8),

% hist([eig_mut48_3(mode(1),:) eig_mut48_3(mode(2),:) eig_mut48_3(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% subplot(6,3,11),

% hist([eig_mut48_4(mode(1),:) eig_mut48_4(mode(2),:) eig_mut48_4(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% subplot(6,3,14),

% hist([eig_mut48_5(mode(1),:) eig_mut48_5(mode(2),:) eig_mut48_5(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% subplot(6,3,17),

% hist([eig_mut48_6(mode(1),:) eig_mut48_6(mode(2),:) eig_mut48_6(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% subplot(6,3,3),

% hist([eig_mut41_1(mode(1),:) eig_mut41_1(mode(2),:) eig_mut41_1(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% title('Mutant R41A')

% subplot(6,3,6),

% hist([eig_mut41_2(mode(1),:) eig_mut41_2(mode(2),:) eig_mut41_2(mode(3),:)],nbins),

% axis([x_beg x_end 0 y_end]);

% 

% saveas(figure1,['Eigenvalues_individualSimulations_',num2str(mode),'.fig']);

%%

% dcd = readdcd('pyd_all6_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_wt_all= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('mut48_all6_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_mut48_all= GNM_fdn(dcd, 10);clear dcd

% dcd = readdcd('mut41_all2_kuru_4to90aligned_eq_CA.dcd', 1:90);

% eig_mut41_all= GNM_fdn(dcd, 10);clear dcd

% %%

% resnum=size(eig_wt_all,1);

% mode=87;

% nbins=50;

% % x_beg=1.5;x_end=8;y_end=1500;%Slowest Eigenvalue

% x_beg=25;x_end=41;y_end=3500;%Fast Eigenvalue

% figure1 = figure('Position',[1000 100 800 800],'Color',[1 1 1]);

% subplot(3,1,1),hist(eig_wt_all(resnum-mode,:),nbins),axis([x_beg x_end 0 y_end]);

% title(['Distribution of Eigenvalues of eigenvector ',num2str(resnum-mode)])

% ylabel('Wild Type')

% subplot(3,1,2),hist(eig_mut48_all(resnum-mode,:),nbins),axis([x_beg x_end 0 y_end]);

% ylabel('Mutant D48A')

% subplot(3,1,3),hist(eig_mut41_all(resnum-mode,:),nbins),axis([x_beg x_end 0 1000]);

% ylabel('Mutant R41A')

% saveas(figure1,['Eigenvalues_allSimulations_',num2str(resnum-mode),'.fig']);

% 

% % myBins = 0:0.5:8; %Fast Eigenvalue

% % % Hists will be the same size because we set the bin locations:

% % y1 = hist(eig_wt_all(resnum-mode,:), myBins);   

% % y2 = hist(eig_mut48_all(resnum-mode,:), myBins);

% % y3 = hist(eig_mut41_all(resnum-mode,:), myBins);

% % % plot the results:

% % figure2 = figure('Position',[1000 100 800 800],'Color',[1 1 1]);

% % bar(myBins, [y1;y2;y3]');

% % title(['Distribution of Eigenvalues of eigenvector ',num2str(resnum-mode)])

% % saveas(figure2,['Eigenvalues_allSimulations_together',num2str(resnum-mode),'.fig']);

% 

%%

dcd = readdcd('pyd_all6_kuru_4to90aligned_eq_CA.dcd', 1:90);

eig_wt_all= GNM_fdn(dcd, 10);clear dcd

dcd = readdcd('mut48_all6_kuru_4to90aligned_eq_CA.dcd', 1:90);

eig_mut48_all= GNM_fdn(dcd, 10);clear dcd

dcd = readdcd('mut41_all6_kuru_4to90aligned_eq_CA.dcd', 1:90);

eig_mut41_all= GNM_fdn(dcd, 10);clear dcd

%% Slowest Modes

myBins_slow1 = 3:0.2:5.5; %Slow Eigenvalue

myBins_slow2 = 3.5:0.2:6.5; %Slow Eigenvalue

myBins_slow3 = 4:0.2:8; %Slow Eigenvalue

% Hists will be the same size because we set the bin locations:

pyd_slow1 = hist(eig_wt_all(89,:), myBins_slow1);

mut48_slow1 = hist(eig_mut48_all(89,:), myBins_slow1);

mut41_slow1 = hist(eig_mut41_all(89,:), myBins_slow1);

pyd_slow2 = hist(eig_wt_all(88,:), myBins_slow2);

mut48_slow2 = hist(eig_mut48_all(88,:), myBins_slow2);

mut41_slow2 = hist(eig_mut41_all(88,:), myBins_slow2);

pyd_slow3 = hist(eig_wt_all(87,:), myBins_slow3);

mut48_slow3 = hist(eig_mut48_all(87,:), myBins_slow3);

mut41_slow3 = hist(eig_mut41_all(87,:), myBins_slow3);

% plot the results:

figure1 = figure('Position',[1000 100 800 800],'Color',[1 1 1]);

subplot(3,1,1)

bar(myBins_slow1, [pyd_slow1;mut48_slow1;mut41_slow1]');legend('wild type','D48A','R41A')

title('Distribution of Eigenvalues of Slowest 3 eigenvector')

title('Slow 1')

ylabel('Frequency')

axis([3 5.5 0 2500]);

subplot(3,1,2)

bar(myBins_slow2, [pyd_slow2;mut48_slow2;mut41_slow2]');

axis([3.5 6.5 0 2500]);

title('Slow 2')

ylabel('Frequency')

subplot(3,1,3)

bar(myBins_slow3, [pyd_slow3;mut48_slow3;mut41_slow3]');

title('Slow 3')

ylabel('Frequency')

xlabel('Eigenvalue Bins')

axis([4 8 0 2500]);

saveas(figure1,'Eigenvalues_allSimulations_Slowest3.fig');

%% Fastest Modes

myBins_fast1 = 30:0.2:41; %Slow Eigenvalue

myBins_fast2 = 30:0.2:42; %Slow Eigenvalue

myBins_fast3 = 30:0.2:42; %Slow Eigenvalue

% Hists will be the same size because we set the bin locations:

pyd_fast1 = hist(eig_wt_all(1,:), myBins_fast1);

mut48_fast1 = hist(eig_mut48_all(1,:), myBins_fast1);

mut41_fast1 = hist(eig_mut41_all(1,:), myBins_fast1);

pyd_fast2 = hist(eig_wt_all(2,:), myBins_fast2);

mut48_fast2 = hist(eig_mut48_all(2,:), myBins_fast2);

mut41_fast2 = hist(eig_mut41_all(2,:), myBins_fast2);

pyd_fast3 = hist(eig_wt_all(3,:), myBins_fast3);

mut48_fast3 = hist(eig_mut48_all(3,:), myBins_fast3);

mut41_fast3 = hist(eig_mut41_all(3,:), myBins_fast3);

% plot the results:

figure1 = figure('Position',[1000 100 800 800],'Color',[1 1 1]);

subplot(3,1,1)

bar(myBins_fast1, [pyd_fast1;mut48_fast1;mut41_fast1]');legend('wild type','D48A','R41A')

title('Distribution of Eigenvalues of Fastest 3 eigenvector')

title('Fast 1')

ylabel('Frequency')

axis([30 42 0 3500]);

subplot(3,1,2)

bar(myBins_fast2, [pyd_fast2;mut48_fast2;mut41_fast2]');

axis([30 43 0 2500]);

title('Fast 2')

ylabel('Frequency')

subplot(3,1,3)

bar(myBins_fast3, [pyd_fast3;mut48_fast3;mut41_fast3]');

title('Fast 3')

ylabel('Frequency')

xlabel('Eigenvalue Bins')

axis([30 43 0 2500]);

saveas(figure1,'Eigenvalues_allSimulations_Fastest3.fig');

%%

resnum=size(eig_wt_all,1);

mode=[89 88 87];

nbins=30;

x_beg=1.5;x_end=8.5;y_end=2500;%Slowest Eigenvalue

% x_beg=25;x_end=41;y_end=3500;%Fast Eigenvalue

figure1 = figure('Position',[1000 100 800 800],'Color',[1 1 1]);

subplot(3,1,1),

hist(eig_wt_all(mode,:)',nbins),

legend('Slowest','Second Slowest','Third Slowest')

% legend('Fastest','Second Fastest','Third Fastest')

axis([x_beg x_end 0 y_end]);

title(['Distribution of Eigenvalues of eigenvector ',num2str(mode)])

ylabel('Wild Type')

subplot(3,1,2),

hist(eig_mut48_all(mode,:)',nbins),

axis([x_beg x_end 0 y_end]);

ylabel('Mutant D48A')

subplot(3,1,3),

hist(eig_mut41_all(mode,:)',nbins),

axis([x_beg x_end 0 y_end]);

ylabel('Mutant R41A')

xlabel('Eigenvalue Bins')

saveas(figure1,['Eigenvalues_allSimulations_',num2str(mode),'.fig']);



