from prody import *
import mkdir_p
import os

def extractLastFrame(pdbFileName,dcdFileName,lastFrame=39):

	# Modified on: 15.10.2014 by Onur and Gulin.
	
	# Importing the trajectory file.
	traj = Trajectory(dcdFileName)

	# Importing the pdb file.
	system = parsePDB(pdbFileName)

	# Linking the trajectory with the pdb.
	traj.link(system)

	# Extracting the last frame from the trajectory.
	lastFrame = traj.getFrame(lastFrame)

	# Generating a file name for last frame pdb without the .dcd extension.
	dcdFileNameNoExt, dcdFileNameExt = os.path.splitext(dcdFileName)
	lastFrameFileName = ('%s_frame_last.pdb' % (dcdFileNameNoExt))

	# Writing the PDB of the last frame to a new file.
	writePDB(lastFrameFileName,lastFrame)

	# Returning the last frame as an AtomGroup object.
	return lastFrame

def splitDCD(traj_tuple):
# Splits a DCD file into individual PDB files.

	# Load the DCD and PDB, then link them together.
	activeSystemString = traj_tuple[0]
	activeTrajString = traj_tuple[1]

	activeSystem = parsePDB(activeSystemString)
	activeTraj = Trajectory(activeTrajString)
	activeTraj.link(activeSystem)
	numFrames = activeTraj.numFrames()

	# Make a directory for split, if it does not exist.
	directoryName = ('%s_splitDCD' % activeTraj.getTitle())
	mkdir_p.mkdir_p(directoryName)
	print 'splitting %s into PDB files now...' % activeTraj.getTitle()

	# Split the DCD and write PDB files to this new directory.
	for i in range(numFrames):
		PDBnameWithFolder = ('%s/%s/%s_frame_%s' % (os.getcwd(),directoryName,activeTraj.getTitle(),
			str(i).zfill(int(len(str(numFrames))))))
		print 'writing into %s...' % PDBnameWithFolder
		writePDB(PDBnameWithFolder,activeTraj.getFrame(i))

	return directoryName
