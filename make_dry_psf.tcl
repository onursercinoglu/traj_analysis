#kuru psf olusturmak icin.
#VMD'yi acip dosyalarimizin oldugu yere geliyoruz.
#mol load psf .....psf pdb .......pdb

set a [atomselect top water]  
set l [lsort -unique [$a get segid]]  
#boylece sildigin waterlarin segilerini aliyoruz sonra

package require psfgen
readpsf ionized.psf
coordpdb  ionized.pdb 
foreach s $l {
delatom $s
}


set b [atomselect top "segname ION"]
set k [lsort -unique [$b get segid]]
foreach d $k {
delatom $d
}

#simdide o segleri sildik
writepsf ionized_dry.psf
writepdb ionized_dry.pdb
#boyle de yazdik