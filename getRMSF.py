#!/usr/bin/env python

#IMPORTS

import sys, argparse, datetime, pandas
from prody import *
import matplotlib.pyplot as plt
import numpy as np

# INPUT PARSING
parser = argparse.ArgumentParser(description='Calculate the Root Mean Square \
	Fluctuation (RMSF) of all residues over the course of a molecular \
	dynamics trajectory (DCD).')

parser.add_argument('pdb',type=str,nargs=1,help='Name of the corresponding pdb \
 file of the DCD trajectory')

parser.add_argument('dcd',type=str,nargs=1,help='Name of the trajectory DCD \
	file')

parser.add_argument('--skip',type=int,default=[1],nargs=1,help='If specified, \
	the trajectory will be handled using skipping every specified number of \
	frames')

parser.add_argument('--superpose',type=str,nargs='+',default=['all'],
	help='A ProDy atom selection string which determines the selection of atoms\
	to be taken into account when doing superposition of the frames of the \
	trajectory')

parser.add_argument('--select',type=str,nargs='+',default=['all'],
	help='A ProDy atom selection string which determines the selection of atoms\
	for which RMSF values should be returned. By default, RMSF of all residues\
	are returned')

parser.add_argument('--refframe',type=int,nargs=1,default=[False],
	help='Number of the frame which should be taken as the reference in \
	superposition')

parser.add_argument('--framerange',type=int,default=[False],nargs='+',
	help='If specified, then only FRAMERANGE section of the trajectory will\
	be handled')

parser.add_argument('--outfile',type=str,nargs=1,default=['RMSF.dat'],
	help='If specified, the results will be written into OUTFILE')

parser.add_argument('--toplot',action='store_true',help='When given, this \
	argument will return a plot of RMSF versus Residues')

args = parser.parse_args()

# METHODS

def getRMSF(pdb,dcd,superpose,select,refFrame,skip,frameRange,outFile,toPlot):

	# Load pdb and dcd files using ProDy
	system = parsePDB(pdb)
	traj = Trajectory(dcd)

	# Get the number of frames
	numFrames = traj.numFrames()

	# Link them
	traj.link(system)

	# Get a selection of "select" atoms and get the number of residues selected
	selected = system.select(select)
	selected_resids = np.unique(selected.getResindices())

	# If frameRange and refFrame are not specified, take default values
	if frameRange[0] == False:
		frames = traj[:]
		if not refFrame:
			refFrame = 0

	# If frameRange is specified and refFrame not, assign the refFrame to the 
	#first frame of frameRange
	elif len(frameRange) > 1 and not refFrame:
		
		refFrame = frameRange[0]
		frames = traj[frameRange[0]:frameRange[1]]
		
	# Set the reference coordinates to the specific frame number coordinates
	traj.setCoords(traj.getFrame(refFrame))

	# Set the working atoms to superposed atoms
	frames.setAtoms(system.select(superpose))

	# Superpose the trajectory on to reference superpose atoms
	frames.superpose()

	# Calculate RMSF based on carbon alpha atoms of select residues
	frames.setAtoms(selected.calpha)
	RMSF = frames.getRMSFs()
	
	# Write the results into outFile
	now = datetime.datetime.now()
	f = open(outFile,'w')
	f.write('# getRMSF.py was run on: \n')
	f.write('# \n')
	f.write('# Date: %d.%d.%d %d:%d \n' % (now.year,now.month,now.day,now.hour,
		now.minute))
	f.write('# \n')
	f.write('# with the following options:\n')
	f.write('# \n')
	f.write('# --pdb\t\t\t%s\n' % pdb)
	f.write('# --dcd\t\t\t%s\n' % dcd)
	f.write('# --superpose\t\t\t%s\n' % superpose)
	f.write('# --skip\t\t\t%d\n' % skip)
	f.write('# --refframe\t\t\t%d\n' % refFrame)
	f.write('# --framerange\t\t\t%s\n' % str(frameRange))
	f.write('# --outfile\t\t\t%s\n' % outFile)
	f.write('# \n')

	f.write('Residue\tRMSF\n')

	for i in range(0,len(RMSF)):
		f.write('%g\t%g\n' % (selected_resids[i],RMSF[i]))

	f.close()

	if toPlot == True:
		plt.plot(RMSF,'b',linewidth=2)
		plt.show()

	return RMSF

# CALLS

if len(args.superpose) > 1:
	superpose = ' '.join(args.superpose)
else:
	superpose = args.superpose[0]

if len(args.select) > 1:
	select = ' '.join(args.select)
else:
	select = args.select[0]

if len(args.framerange) > 1:
	framerange = np.asarray(args.framerange)
else:
	framerange = args.framerange

getRMSF(pdb=args.pdb[0],dcd=args.dcd[0],superpose=superpose,select=select,
	refFrame=args.refframe[0],skip=args.skip[0],frameRange=framerange,
	outFile=args.outfile[0],toPlot=args.toplot)
