from prody import *
import numpy as np
import matplotlib.pyplot as plt
import itertools
import multiprocessing as mp
import pyprind

sourceSel = 'all'
targetSel = 'all'

cutoff = 20

traj = Trajectory('/home/onur/Dropbox/repos_test_files/test.dcd')
system = parsePDB('/home/onur/Dropbox/repos_test_files/test.pdb')

traj.link(system)
traj.setCoords(traj.getFrame(0))
traj.setAtoms(system.select('name CA'))

numFrames = traj.numFrames()

# Get source selection residues.
source = system.select(sourceSel)
sourceCA = source.select('calpha')
numSource = len(sourceCA)
sourceResids = sourceCA.getResindices()
sourceResnums = sourceCA.getResnums()
sourceSegnames = sourceCA.getSegnames()

allResidues = system.select('all')
allResiduesCA = allResidues.select('calpha')
numResidues = len(allResiduesCA)
numTarget = numResidues

# By default, targetResids are all residues.
targetResids = np.arange(numResidues)
	
# Get target selection residues, if provided:
if targetSel:
	target = system.select(targetSel)
	targetCA = target.select('calpha')
	numTarget = len(targetCA)
	targetResids = targetCA.getResindices()

# Get all combinations of sourceResids-targetResids
allCombins = list(itertools.product(sourceResids,targetResids))

# Get only unique combinations as a set
uniqueCombins = set(tuple(map(tuple,map(sorted,tuple(allCombins)))))
uniqueCombins = list(uniqueCombins)

# For each sourceResid and targetResid, determine
# whether they get within cutoff distance to each other
# along the trajectory
progBar = pyprind.ProgPercent(len(uniqueCombins))

for j in range(0,len(uniqueCombins)):
	sel0 = 'resindex %d' % sourceResids[uniqueCombins[j][0]]
	sel1 = 'resindex %d' % sourceResids[uniqueCombins[j][1]]
	
	if sel0 != sel1:

		for i in range(0,numFrames):

			activeFrame = traj.getFrame(i)
			#activeFrame.superpose()
			activeAtoms = activeFrame.getAtoms()

			distance = calcDistance(activeAtoms.select(sel0),activeAtoms.select(sel1))
			
			# If the distance is below cutoff, then include this pair and continue to
			# next step in for loop.
			if distance < cutoff:
				print 'include pair', uniqueCombins[j]
				break

	progBar.update(j/float(len(uniqueCombins))*float(100))


		#plt.plot(distance[j][k])
		#plt.show()