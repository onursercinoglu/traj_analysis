#!/usr/bin/env python

# IMPORTS
import argparse
import pickle
import matplotlib.pyplot as plt
import pandas
import numpy as np
import networkx as nx

# INPUT PARSING
parser = argparse.ArgumentParser(description='Find possible allosteric paths \
	from energy correlations output from getEnergyCorrs.py')

parser.add_argument('corrFile',type=str,nargs=1,help='Name of corresponding \
	output file from energyCorrs.py')

parser.add_argument('sourceres',type=int,nargs=1,help='sourceres')

parser.add_argument('targetres',type=int,nargs=1,help='targetres') 

parser.add_argument('--pickle',type=str,nargs=1,default=[False])

parser.add_argument('--chainends',type=int,nargs=2,help='Chain end residues')

parser.add_argument('--r2cutoff',type=float,nargs=1,default=[0.8],help='R2 cutoff for correlation') 

args = parser.parse_args()

corrFile = args.corrFile[0]
sourceres = args.sourceres[0]
pickleFile = args.pickle[0]
targetres = args.targetres[0]

chainEnds = args.chainends

r2cutoff = args.r2cutoff[0]

# Load the correlation list	
corrList = pandas.read_table(corrFile,header=0)

# Start the undirected graph.
graph = nx.Graph()

# For each member in the list, check whether there is a bridge connection
# e.g. [13,34] and [34,56] or [13,34] and [35,59] (second one to account for covalent contacts)
for i in range(0,len(corrList)):

	# Load the Rsq value.
	Rsq = corrList['R2'][i]

	# If it is below the r2cutoff value, then skip to next correlation.
	if Rsq < r2cutoff:
		continue

	# Load contact correlation pairs
	corrPair1 = [corrList['Source1'][i],corrList['Source2'][i]]
	corrPair2 = [corrList['Target1'][i],corrList['Target2'][i]]

	# Load all residues in this contact correlation pair and add them as edges to the graph.
	source1 = corrList['Source1'][i]
	graph.add_node(source1)
	source2 = corrList['Source2'][i]
	graph.add_node(source2)
	target1 = corrList['Target1'][i]
	graph.add_node(target1)
	target2 = corrList['Target2'][i]
	graph.add_node(target2)

	# Check whether there is a bridge residue among these correlations.
	bridges = [x for x in corrPair1 if x in corrPair2]

	# If no bridge residue is found, continue to next correlation pair
	if not bridges:
		continue
	# If a bridge is found, determine ends of this contact, then connect the bridge and these two ends.
	else:
		bridge = bridges[0]
		end1 = [x for x in corrPair1 if x != bridge][0]
		end2 = [x for x in corrPair2 if x != bridge][0]
		graph.add_edge(end1,bridge)
		graph.add_edge(bridge,end2)

# Connect residues that are covalently bonded.
for node in graph.nodes():
	if not node in chainEnds:
		graph.add_edge(node,node+1)

# Work the magic and find all shortest paths between specified sourceres and targetres!
paths = [p for p in nx.all_shortest_paths(graph,source=sourceres,target=targetres)]

print paths

if pickleFile and paths:
	# If energies pickle is provided, load it.
	energies = pickle.load(open(pickleFile,'r'))
	energiesTotal = energies['Total']

	for p in paths:
		f, ax = plt.subplots(2,len(p)-1)

		# This is a stupid correction but necessary :()
		#if len(p) == 3:
		#	ax = [ax]

		for i in range(0,len(p)-2):
			label1 = map(str,[p[i],p[i+1]])
			ax[0][i].plot(energiesTotal[p[i]][p[i+1]],'black',label=label1)
			label2 = map(str,[p[i+1],p[i+2]])
			ax[0][i].plot(energiesTotal[p[i+1]][p[i+2]],'r',label=label2)
			# ax[i].plot(energiesTotal[p[i]][p[i+1]],energiesTotal[p[i+1]][p[i+2]],'o')
			ax[0][i].set_xlabel(map(int,[p[i],p[i+1]]))
			ax[0][i].set_ylabel(map(int,[p[i+1],p[i+2]]))

			ax[1][i].plot(energiesTotal[p[i]][p[i+1]],energiesTotal[p[i+1]][p[i+2]],'bo')
					
		plt.title('kcal/mol')
		plt.show()