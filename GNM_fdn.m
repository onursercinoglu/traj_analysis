% clc, clear all, close all
% dcd = readdcd('pyd_6_100ns_kuru_4to90aligned_eq_CA.dcd', 1:90);
%eig_pyd_6= GNM_fdn(dcd, 10);

function w= GNM_fdn(dcd, rcut)
frameCount =size(dcd,1);
alphaCount = size(dcd,2)/3;
r = zeros(alphaCount, 3, frameCount);
for i=1:frameCount
    for j=1:alphaCount
        r(j,:,i)= [dcd(i,(3*j-2)) dcd(i,(3*j-1)) dcd(i,(3*j))];
    end
end
D=zeros(alphaCount, alphaCount,1);
contact=zeros(alphaCount,alphaCount);
U=zeros(alphaCount,alphaCount);
S=zeros(alphaCount,alphaCount);
V=zeros(alphaCount,alphaCount);
w=zeros(alphaCount,frameCount);
for f = 1:frameCount
    D(:,:,f)=pdist2(r(:,:,f),r(:,:,f));
    contact(((D(:,:,f) <= rcut).* (D(:,:,f) > 0.0001))==1)=-1;
    contact = contact - diag(sum(contact));
    [U,S,V]=svd(contact);
    w(:,f)=diag(S);
end
% figure1 = figure('Position',[1000 100 800 800],'Color',[1 1 1])
% nbins=50;
% title('Distribution of Eigenvalues')
% subplot(6,2,1),hist(eig_pyd_1(:),nbins),