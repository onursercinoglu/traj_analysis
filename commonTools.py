aaCodeNames = [['Alanine','Ala','A'],
['Cysteine','Cys','C'],
['Aspartic Acid','Asp','D'],
['Glutamic Acid','Glu','E'],
['Phenylalanine','Phe','F'],
['Glycine','Gly','G'],
['Histidine','His','H'],
['Isoleucine','Ile','I'],
['Lysine','Lys','K'],
['Leucine','Leu','L'],
['Methionine','Met','M'],
['Asparagine','Asn','N'],
['Proline','Pro','P'],
['Glutamine','Gln','Q'],
['Arginine','Arg','R'],
['Serine','Ser','S'],
['Threonine','Thr','T'],
['Valine','Val','V'],
['Tryptophan','Trp','W'],
['Unknown','Xaa','X'],
['Tyrosine','Tyr','Y']]

def getOneLetter(aaString):
	
	if len(aaString) == 1:
		k = 2
	elif len(aaString) == 3:
		k = 1
	else:
		k = 0

	for i in range(0,len(aaCodeNames)):
		if aaCodeNames[i][k].upper() == aaString.upper():
			oneLetter = aaCodeNames[i][2]

	return oneLetter

def getThreeLetter(aaString):
	
	if len(aaString) == 1:
		k = 2
	elif len(aaString) == 3:
		k = 1
	else:
		k = 0

	for i in range(0,len(aaCodeNames)):
		if aaCodeNames[i][k] == aaString:
			threeLetter = aaCodeNames[i][1]

	return threeLetter

def getFullName(aaString):
	
	if len(aaString) == 1:
		k = 2
	elif len(aaString) == 3:
		k = 1
	else:
		k = 0

	for i in range(0,len(aaCodeNames)):
		if aaCodeNames[i][k] == aaString:
			fullName = aaCodeNames[i][0]

	return fullName