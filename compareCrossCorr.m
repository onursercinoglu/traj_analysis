function signCompareCrossCorr = compareCrossCorr(crossCorr1,crossCorr2,varargin)

%% Modified on: 04.07.2014 by Onur Serçinoğlu

%% Compare the two cross-correlation matrices.
% binaryCompareCrossCoor only gives the correlation direction changes. 
% i.e. if two residues switch from a - to + correlation, it gives -1, else
% it gives 1, so you should focus on -1 values in binaryCompareCrossCorr.
if ~isempty(crossCorr1(crossCorr1==0))
    crossCorr1(crossCorr1==0) = 1e-10;
end
if ~isempty(crossCorr2(crossCorr2==0))
    crossCorr2(crossCorr2==0) = 1e-10;
end

signCompareCrossCorr = (crossCorr1.*crossCorr2)./sqrt((crossCorr1.^2).*(crossCorr2.^2));
magnitudeCompareCrossCorr = abs(crossCorr1-crossCorr2);

% Experimental!!
magnitudeThreshold = 0.8;
magnitudeCompareCrossCorr(magnitudeCompareCrossCorr<magnitudeThreshold)=0;
% Experimental''

compareCrossCorr = magnitudeCompareCrossCorr.*signCompareCrossCorr;

%% Parse optional inputs.
p = inputParser;
p.addParamValue('toPlot',true);
p.addParamValue('toMat',true);
p.addParamValue('toFig',true);

p.parse(varargin{:});
toPlot = p.Results.toPlot;
%toMat = p.Results.toMat;
%toFig = p.Results.toFig;

%% Plot the result if the user wanted to.
if toPlot==true;
    figure; imagesc(compareCrossCorr);set(gca,'YDir','normal');colormap(bluewhitered);colorbar;
    set(gca,'XTick',(0:20:length(compareCrossCorr)));
    set(gca,'YTick',(0:20:length(compareCrossCorr)));
end