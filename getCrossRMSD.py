#!/usr/bin/env python

# IMPORTS

from prody import *
import matplotlib.pyplot as plt
import numpy as np
import argparse
import pyprind

# INPUT PARSING

parser = argparse.ArgumentParser(description='Get the cross RMSD matrix of frames in trajectory.')

parser.add_argument('pdb',type=str,nargs=1,help='Name of the corresponding pdb \
 file of the DCD trajectory')

parser.add_argument('dcd',type=str,nargs=1,help='Name of the trajectory DCD \
	file')

parser.add_argument('--outfile',type=str,nargs=1,default=['crossRMSD.dat'],
	help='If specified, the results will be written into OUTFILE')

parser.add_argument('--toplot',action='store_true',help='When given, this \
	argument will return a heatmap of cross RMSD values')

args = parser.parse_args()

# METHODS

def getCrossRMSD(pdb,dcd,outFile,toPlot):

	# Load the trajectory and link it with PDB
	traj = Trajectory(dcd)
	system = parsePDB(pdb)
	traj.link(system)
	
	# Get the number of frames and start a numpy matrix for storing RMSD values.
	numFrames = traj.numFrames()
	crossRMSD = np.zeros((numFrames,numFrames))

	progBar = pyprind.ProgBar(numFrames)

	for i in range(0,numFrames):
		traj.setCoords(traj.getFrame(i))
		
		for k in range(0,numFrames):
			activeFrame = traj.getFrame(k)
			traj.setAtoms(system.select('name CA'))
			activeFrame.superpose()
			crossRMSD[i][k] = activeFrame.getRMSD()
			traj.setAtoms(system.select('all'))

		progBar.update()

	np.savetxt(outFile,crossRMSD,delimiter='\t')

# CALLS

getCrossRMSD(args.pdb[0],args.dcd[0],outFile=args.outfile[0],toPlot=args.toplot)


