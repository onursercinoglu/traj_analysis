import os
import re
import csv

def listXSC(folderName):

	fw = open('xscList.csv','wb')
	writer = csv.writer(fw,delimiter='\t')
	writer.writerow(['Directory','File Name','Last Time Step'])
	for root, dirs, files in os.walk(folderName):
		for filename in files:
			if (filename.endswith(('.xsc')) and len(re.findall('alanin.initial',filename)) == 0):
				fr = open(os.path.join(root,filename),'r')
				inputData = fr.readlines()
				if len(inputData) > 2:
					lastLine = inputData[2].split()
					lastTimeStep = lastLine[0]
					#print filename, lastTimeStep
					writer.writerow([root,filename,lastTimeStep])				
					#print '%s/%s\t%s' % (root,filename,lastTimeStep)
				fr.close()
	fw.close()