import re
import numpy as np
import copy
from prody import *
import os
import csv
import time
import datetime
from subprocess import call
import glob
import multiprocessing

def batchProbe(inputFolder=os.getcwd(),outputFolder=os.getcwd(),flags='-self',selection='',probeAlias='probe'):
# This batch PROBEs a list of PDB files contained in a specified folder.
# PROBE is an external program, developed by Michael Word in 1999. It must be installed and available for call via bash to the use of this method.

	# Obtain the names of all PDB files in the inputFolder.
	pdbFileNames = glob.glob('*.pdb')
	numPDBfiles = len(pdbFileNames)

	probeOutputCounter = 0
	# Run probe for all PDB files that are scanned.
	for pdbFileName in pdbFileNames:
		print ('%s percent completed' % str(float(probeOutputCounter)*float(100)/float(numPDBfiles)))
		#callArgs = [probeAlias,('-%s' % 'self'),selection,pdbFileName,'>>',('%s.kin' % pdbFileName)]
		callString = (('%s %s %s -kinemage %s -quiet >> %s_%s_%s.kin') % (probeAlias,flags,selection,pdbFileName,pdbFileName,selection,flags))
		#print callString
		call(callString,shell=True)
		callString = (('%s %s %s -unformated -condense %s -quiet >> %s_%s_%s_condense.kin') % (probeAlias,flags,selection,pdbFileName,pdbFileName,selection,flags))
		#print callString
		call(callString,shell=True)

		probeOutputCounter = probeOutputCounter+1

def batchReduce(inputFolder=os.getcwd(),outputFolder=os.getcwd(),flags=['FLIP'],reduceAlias='reduce'):
# This batch REDUCEs a list of PDB files contained in a specified folder.
# REDUCE is an external program, developed by Michael Word in 1999. It must be installed and available for call via bash to the use of this method.

	# Obtain the names of all PDB files in the inputFolder.
	pdbFileNames = glob.glob('*.pdb')
	numPDBfiles = len(pdbFileNames)

	# Run REDUCE for all PDB files that are scanned.
	for pdbFileName in pdbFileNames:
		#callArgs = [reduceAlias,'-quiet',('-%s' % 'FLIP'),pdbFileName,'>>',('%s_withH.pdb' % pdbFileName)]
		callString = (('%s -%s -%s %s >> %s_withH.pdb') % (reduceAlias,'quiet','FLIP',pdbFileName,pdbFileName))
		call(callString,shell=True)

# def batchPrekin(inputFolder=os.getcwd(),outputFolder=os.getcwd(),prekinAlias='prekin-nogui'):
# # This batch PREKINs a list of PDB files contained in a specified folder.
# # PREKIN is an external program, developed by Michael Word in 1999. It must be installed and available for call via bash to the use of this method.

# 	# Obtain the names of all PDB files in the inputFolder.
# 	pdbFileNames = glob.glob('*.pdb')
# 	numPDBfiles = len(pdbFileNames)

# 	# Run prekin for all PDB files that are scanned.
# 	for pdbFileName in pdbFileNames:
# 		callArgs = (('%s %s >> %s.kin') % (prekinAlias,pdbFileName,pdbFileName))
# 		print callString
# 		call(callString)

def getProbeHeatMap(pdbFileName,probeOutputs):

	# Create the heat map matrix.
	pdbSystem = parsePDB(pdbFileName)
	numAtoms = pdbSystem.numAtoms()
	numResidues = pdbSystem.numResidues()
	probeHeatMap = np.zeros((numAtoms,numAtoms))

	# Scan through probeOutputs and assign dot-counts by atom name pairs.
	srcAtom = {'chain':[],'resnum':[],'resname':[],'atomname':[]}
	targAtom = {'chain':[],'resnum':[],'resname':[],'atomname':[]}
	
	probeOutputCounter = 0
	for probeOutput in probeOutputs:
		print float(probeOutputCounter),float(100),float(len(probeOutputs))
		print '( %s %s completed) Getting dot-counts for a probeOutput' % (str(float(probeOutputCounter)*float(100)/float(len(probeOutputs))),'%')
		for i in xrange(0,len(probeOutput['name'])):
			# Extract the source atom and target atom properties
			srcAtomMatches = re.search('\s(\D+)\s*(\d+)\s(\D\D\D)\s\s(\S+)',probeOutput['srcAtom'][i])
			srcAtomMatches = srcAtomMatches.group().split()
			srcAtom['chain'] = srcAtomMatches[0]
			srcAtom['resnum'] = srcAtomMatches[1]
			srcAtom['resname'] = srcAtomMatches[2]
			srcAtom['atomname'] = srcAtomMatches[3]
			targAtomMatches = re.search('\s(\D+)\s*(\d+)\s(\D\D\D)\s\s(\S+)',probeOutput['targAtom'][i])
			targAtomMatches = targAtomMatches.group().split()
			targAtom['chain'] = targAtomMatches[0]
			targAtom['resnum'] = targAtomMatches[1]
			targAtom['resname'] = targAtomMatches[2]
			targAtom['atomname'] = targAtomMatches[3]

			# Find their indices.
			# print 'chain %s resnum %s name %s' % (srcAtom['chain'],srcAtom['resnum'],srcAtom['atomname'])
			srcInPdb = pdbSystem.select('chain %s resnum %s name %s' % (srcAtom['chain'],srcAtom['resnum'],srcAtom['atomname']))
			srcIndex = srcInPdb.getIndices()[0]
			targInPdb = pdbSystem.select('chain %s resnum %s name %s' % (targAtom['chain'],targAtom['resnum'],targAtom['atomname']))
			targIndex = targInPdb.getIndices()[0]

			# Accumulate heat map dot-count values.
			probeHeatMap[srcIndex,targIndex] = probeHeatMap[srcIndex,targIndex] + probeOutput['dot-count'][i]
			probeHeatMap[targIndex,srcIndex] = probeHeatMap[targIndex,srcIndex] + probeOutput['dot-count'][i]

		probeOutputCounter = probeOutputCounter+1

	# Truncate the heatmap to a residue-based version to save up on memory.
	probeHeatMapResidues = np.zeros((numResidues,numResidues))
	iterResiduesSrc = pdbSystem.iterResidues()
	
	print 'Truncating the atom-based heat map to a residue-based version now...'
	for i in xrange(0,numResidues-1):
		print 'Truncating for residue %i now...' % i
		srcResidueInPdb = iterResiduesSrc.next()
		srcResidueAtomIndices = srcResidueInPdb.getIndices()
		iterResiduesTarg =pdbSystem.iterResidues()
		for m in xrange(0,numResidues-1):
			targResidueInPdb = iterResiduesTarg.next()
			targResidueAtomIndices = targResidueInPdb.getIndices()
			probeHeatMapResidues[i,m] = np.sum(np.vstack(probeHeatMap[np.ix_(srcResidueAtomIndices,targResidueAtomIndices)]))
			probeHeatMapResidues[m,i] = np.sum(np.vstack(probeHeatMap[np.ix_(srcResidueAtomIndices,targResidueAtomIndices)]))
			# HERE YOU NEED TO CALL re.search (OR SOME OTHER METHOD OF re...) and extract chain, resnum, resname and atomname
			# values for both srcAtom and targAtom from the probeOutput.
			# Then you need to find the indices of interacting atoms. 
			# After that, you need to create another problem 
	
	return probeHeatMapResidues
	

def mergeProbeOutputs(regExpProbeOutputFiles):
# THis reads all PROBE Outputs files that match the regExpProbeOutputFiles regular expression 
# by calling readProbeOutput, then merges them in a single list.
	probeOutputFileNames = glob.glob(regExpProbeOutputFiles)
	probeOutputFileNames.sort()
	probeOutputs = list()
	
	for fileName in probeOutputFileNames:
		probeOutput = readProbeOutput(fileName)
		probeOutputs.append(probeOutput)

	return probeOutputs

def readProbeOutput(probeOutputFileName):
# This reads a .kin output file of PROBE and assigns the contents to a dictionary with appropriate field names.
	probeOutput = {'name':[],'pat':[],'type':[],'srcAtom':[],'targAtom':[],'dot-count':[],
		'mingap':[],'gap':[],'spX':[],'spY':[],'spZ0':[],'spikeLen':[],'score':[],'stype':[],
		'ttype':[],'x':[],'y':[],'z':[],'sBval':[],'tBval':[]}

	f = open(probeOutputFileName,'r')
	reader = csv.reader(f,delimiter=':')
	
	for line in reader:
		#line = line.strip()
		probeOutput['nam/e'].append(line[0])
		probeOutput['pat'].append(line[1])
		probeOutput['type'].append(line[2])
		probeOutput['srcAtom'].append(line[3])
		probeOutput['targAtom'].append(line[4])
		probeOutput['dot-count'].append(float(line[5]))
		probeOutput['gap'].append(float(line[7]))
		probeOutput['spX'].append(float(line[8]))
		probeOutput['spY'].append(float(line[9]))
		probeOutput['spZ0'].append(float(line[10]))
		probeOutput['spikeLen'].append(float(line[11]))
		probeOutput['score'].append(float(line[12]))
		probeOutput['stype'].append(line[13])
		probeOutput['ttype'].append(line[14])
		probeOutput['x'].append(float(line[15]))
		probeOutput['y'].append(float(line[16]))
		probeOutput['z'].append(float(line[17]))
		probeOutput['sBval'].append(float(line[18]))
		probeOutput['tBval'].append(float(line[19]))

	return probeOutput