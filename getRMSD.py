#!/usr/bin/env python

#IMPORTS

import sys, argparse, datetime, pandas, pyprind
import prody
import matplotlib.pyplot as plt
import numpy as np

# METHODS

def getRMSD(pdb,dcd,superpose='all',refFrame=0,skip=1,movAvg=False,outFile='RMSD.dat',toPlot=False):
	
	# Load pdb and dcd files using ProDy
	if type(pdb) == str:
		system = prody.parsePDB(pdb)
	else:
		system = pdb

	if type(dcd) == str:
		traj = prody.Trajectory(dcd)
	else:
		traj = dcd

	# Link them
	traj.link(system)

	# Get the number of frames
	numFrames = traj.numFrames()

	# Set the reference coordinates to the specific frame number coords
	traj.setCoords(traj.getFrame(refFrame))

	# Set the working atoms to superposed atoms
	traj.setAtoms(system.select(superpose))

	# Calculate and accumulate the RMSD values, taking also skip into account
	frameRange = np.arange(0,numFrames,skip)
	RMSD = np.zeros(len(frameRange))

	progBar = pyprind.ProgBar(len(frameRange))

	for i in range(0,len(frameRange)):
		activeFrame = traj.getFrame(frameRange[i])
		activeFrame.superpose()
		RMSD[i] = activeFrame.getRMSD()

		progBar.update()

	# If moving average is requested, calculate and return it
	if movAvg:
		RMSD_ma = pandas.rolling_mean(RMSD,movAvg)

	# Write the results into outFile
	now = datetime.datetime.now()
	f = open(outFile,'w')
	f.write('# getRMSD.py was run on: \n')
	f.write('# \n')
	f.write('# Date: %d.%d.%d %d:%d \n' % (now.year,now.month,now.day,now.hour,
		now.minute))
	f.write('# \n')
	f.write('# with the following options:\n')
	f.write('# \n')
	f.write('# --pdb\t\t\t%s\n' % pdb)
	f.write('# --dcd\t\t\t%s\n' % dcd)
	f.write('# --superpose\t\t\t%s\n' % superpose)
	f.write('# --skip\t\t\t%d\n' % skip)
	f.write('# --refframe\t\t\t%d\n' % refFrame)
	f.write('# --movavg\t\t\t%d\n' % movAvg)
	f.write('# --outfile\t\t\t%s\n' % outFile)
	f.write('# \n')

	if not movAvg:
		f.write('Frame\tRMSD\n')
	else:
		f.write('Frame\tRMSD\tRMSD_movavg\n')

	for i in range(0,len(frameRange)):
		if not movAvg:
			f.write('%g\t%g\n' % (frameRange[i],RMSD[i]))
		else:
			f.write('%g\t%g\t%g\n' % (frameRange[i],RMSD[i],RMSD_ma[i]))

	f.close()

	if toPlot == True:
		plt.plot(RMSD,'b')
		if movAvg:
			plt.plot(RMSD_ma,'k',linewidth=2)
		plt.show()

	return RMSD

if __name__ == '__main__':

	# INPUT PARSING
	parser = argparse.ArgumentParser(description='Calculate the Root Mean Square \
		Deviation (RMSD) of (selected) residues over the course of a molecular \
		dynamics trajectory (DCD).')

	parser.add_argument('pdb',type=str,nargs=1,help='Name of the corresponding pdb \
	 file of the DCD trajectory')

	parser.add_argument('dcd',type=str,nargs=1,help='Name of the trajectory DCD \
		file')

	parser.add_argument('--skip',type=int,default=[1],nargs=1,help='If specified, \
		the trajectory will be handled using skipping every specified number of \
		frames, leading to a faster overall calculation at the cost of losing \
		frames')

	parser.add_argument('--superpose',type=str,nargs='+',default=['all'],
		help='A ProDy atom selection string which determines the selection of atoms\
		to be taken into account when doing superposition of the frames of the \
		trajectory')

	parser.add_argument('--refframe',type=int,nargs=1,default=[0],
		help='Number of the frame which should be taken as the reference in \
		superposition')

	# MAYBE TO BE ADDED LATER, NOW DEFUNCT.
	#parser.add_argument('--select',type=str,nargs='+',default='all',
	#	help='A ProDy atom selection string. If specified, then the RMSD of only \
	#	these selected atoms will be returned')

	parser.add_argument('--outfile',type=str,nargs=1,default=['RMSD.dat'],
		help='If specified, the results will be written into OUTFILE')

	parser.add_argument('--movavg',type=int,default=False,nargs=1,help='If \
		specified a moving average (rolling mean) of RMSD values using windows of \
		size MOVAVG will additionally be returned')

	parser.add_argument('--toplot',action='store_true',help='When given, this \
		argument will return a plot of RMSD versus Frames')

	args = parser.parse_args()

	# CALLS

	if len(args.superpose) > 1:
		superpose = ' '.join(args.superpose)
	else:
		superpose = args.superpose[0]

	if args.movavg:
		movavg = args.movavg[0]
	else:
		movavg = args.movavg

	# Load the trajectory now.
	dcd = prody.Trajectory(args.dcd[0])

	# Parse the PDB file now.
	pdb = prody.parsePDB(args.pdb[0])

	getRMSD(pdb=pdb,dcd=dcd,superpose=superpose,
		refFrame=args.refframe[0],skip=args.skip[0],movAvg=movavg,
		outFile=args.outfile[0],toPlot=args.toplot)
