set outfile [open rgyr.dat w];                                         
set nf [molinfo top get numframes]
set sel [atomselect top "protein and alpha"]

#rgyr calculation loop
for { set i 0 } {$i < $nf } { incr i } {
    $sel frame $i 
    $sel update 
    puts $outfile "$i [measure rgyr $sel]" 
}
$sel delete

close $outfile