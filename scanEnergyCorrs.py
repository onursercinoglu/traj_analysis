#!/usr/bin/env python

# IMPORTS
import argparse
import pickle
import matplotlib.pyplot as plt
import pandas

# INPUT PARSING
parser = argparse.ArgumentParser(description='Plot sequence of correlated \
	interactions from pickled energy file and correlation files.')

parser.add_argument('pickle',type=str,nargs=1,help='Name of the corresponding \
	pickle file')

parser.add_argument('filteredCorrFile',type=str,nargs=1,help='Name of corresponding \
	filtered energies file')

parser.add_argument('--res',type=int,nargs=1,default=[False],help='Optional residue number \
	for finding interactions') 

args = parser.parse_args()

pickleFile = args.pickle[0]
filteredCorrFile = args.filteredCorrFile[0]
res = args.res[0]

def scanEnergyCorrs(pickleFile,filteredCorrFile,res):

	energies = pickle.load(open(pickleFile,'r'))
	energiesTotal = energies['Total']

	corrList = pandas.read_table(filteredCorrFile,header=0)

	for i in range(0,len(corrList)):
		source1 = corrList['Source1'][i]
		source2 = corrList['Source2'][i]
		target1 = corrList['Target1'][i]
		target2 = corrList['Target2'][i]
		if res:
			if res not in [source1,source2,target1,target2]:
				continue
				
		plt.plot(energiesTotal[source1][source2],energiesTotal[target1][target2],'o')
		plt.xlabel(map(int,[source1,source2]))
		plt.ylabel(map(int,[target1,target2]))
		plt.title(int(i/float(len(corrList)*100)))
		plt.show()

# CALLS
scanEnergyCorrs(pickleFile,filteredCorrFile,res)
