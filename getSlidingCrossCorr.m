function getSlidingCrossCorr(varargin)

% Parse input parameters
p = inputParser;
p.addRequired('dcdname',@(x)ischar(x));
p.addRequired('res2read',@(x)isnumeric(x));
p.addParamValue('wsize',@(x)isnumeric(x));
p.addParamValue('NoWorkers',@(x)isnumeric(x));

p.parse(varargin{:});

dcd_filename = p.Results.dcdname;
res2read = p.Results.res2read;
windowsize = p.Results.wsize;
noworkers = p.Results.NoWorkers;

%% Read the dcd.
dcd = readdcd(dcd_filename,res2read);

frameCount = size(dcd,1);
alphaCount = size(dcd,2)/3;

frameChunks = linspace(1,frameCount,noworkers);

matlabpool(noworkers)

corrs = cell(noworkers);
r = cell(noworkers);
c = cell(noworkers);
r_mean = cell(noworkers);
unnorm_corr = cell(noworkers);
corr = cell(noworkers);

parfor n = 1:noworkers-1
    
    frameChunk = frameChunks(n):(frameChunks(n+1)-windowsize);
    corrs{n} = cell(length(frameChunk))
    r{n} = cell(length(frameChunk));
    c{n} = cell(length(frameChunk));
    r_mean{n} = cell(length(frameChunk));
    unnorm_corr{n} = cell(length(frameChunk));
    corr{n} = cell(length(frameChunk));
    
    for i = frameChunk(1):frameChunk(end)
        disp(['step ' i])
        r{n}{i} = zeros(alphaCount,3,windowsize);
        % Per window, get coordinates and mean coordinates.
        c{n}{i} = 1;
        for j = i:i+windowsize
            
            for k = 1:alphaCount
                r(k,:,c) = [dcd(j,(3*k-2)) dcd(j,(3*k-1)) dcd(j,(3*k))];
            end
            r_mean{n}{i} = mean(r{n}{i},3);
            
            c{n}{i} = c{n}{i} + 1;
        end
        
        unnorm_corr{n}{i} = zeros(alphaCount,alphaCount);
        
        for f = 1:windowsize
            unnorm_corr{n}{i} = unnorm_corr{n}{i} + (r(:, :, f) - r_mean{n}{i}) * (r(:, :, f) - r_mean{n}{i})' / (windowsize );
        end
        
        corr{n}{i} = unnorm_corr{n}{i} ./ sqrt(diag(unnorm_corr{n}{i}) * diag(unnorm_corr{n}{i})');
        
        % contourf(corr,'LineStyle','none');
        % xlabel('Residue number','FontSize',15);ylabel('Residue number','FontSize',15);
        
        % filename_suffix = [num2str(res2read(1)) '_' num2str(res2read(end))];
        % filename = [dcd_filename '_' filename_suffix];
        % title([filename ' Cross Correlations'],'FontSize',18,'interpreter','none')
        % axis square;colorbar;caxis([-1 1]);
        % set(gca,'XTick',(0:20:alphaCount));
        % set(gca,'YTick',(0:20:alphaCount));
        % set(gcf, 'Color', 'w')
        % drawnow
        
        % corrs{i} = corr;
        
    end
    
end

matlabpool close

end