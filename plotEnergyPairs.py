#!/usr/bin/env python

# IMPORTS
import argparse
import pickle
import matplotlib.pyplot as plt
import pandas
import numpy as np

# INPUT PARSING
parser = argparse.ArgumentParser(description='Plot energies of selected two residues from pickle')

parser.add_argument('pickle',type=str,nargs=1,help='pickle file')

parser.add_argument('sourcepair',type=int,nargs=2,help='sourcepair')

parser.add_argument('targetpair',type=int,nargs=2,help='targetpair')

args = parser.parse_args()

pickleFile = args.pickle[0]

sourcePair = args.sourcepair

targetPair = args.targetpair

# Load energies
energies = pickle.load(open(pickleFile,'r'))
energiesTotal = energies['Total']

f, ax = plt.subplots(1,2)
ax[0].plot(energiesTotal[sourcePair[0]][sourcePair[1]],'b')
ax[0].plot(energiesTotal[targetPair[0]][targetPair[1]],'r')
ax[1].plot(energiesTotal[sourcePair[0]][sourcePair[1]],energiesTotal[targetPair[0]][targetPair[1]],'o')

plt.show()