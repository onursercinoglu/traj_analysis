#!/usr/bin/env python

# IMPORTS

import argparse
from namdConf import *
from prody import *
import copy
import re

# INPUT PARSING
parser = argparse.ArgumentParser(description='Quickly write a new NAMD configuration file to \
	"resume" a previously interrupted/finished NAMD simulation chunk.')

parser.add_argument('prevconf',type=str,nargs=1,help='Name of the previous conf file')
parser.add_argument('finalstep',type=int,nargs=1,help='Final time step for the new conf file')
parser.add_argument('--outputname',nargs=1,default=False,help='Name of the new conf file.\
	If not specified, an automatically generated file name in the form of \
	restart_$firsttimestep_$finaltimestep.conf is used where $firsttimestep \
	is the last time step of the previous conf file and $finaltimestep is the \
	time step supplied in the finalstep argument')
parser.add_argument('--mode',type=str,nargs=1,choices=['run','min'],default='run',
	help='Mode of the new simulation. \
	Mode can be either run or min. ''run'' means a regular molecular dynamics simulation is to \
	be started. ''min'' means a minimization should be performed')

args = parser.parse_args()

# METHODS

def continueConf(confFileName,finalTimeStep,outputName=False,mode='run'):

	prevConf =  namdConf()
	prevConf.readConf(confFileName)

	# Here scan for the .restart.xsc with the outputname of the prevConf.
	targetTimeStep = prevConf.run

	# Here get the outputname, then read the last time step from its restart.xsc file.
	prevOutputName = prevConf.outputName
	if prevOutputName.startswith('$'):
		matches = re.search(r'\$(\S+)',prevOutputName)
		setVarName = matches.group(1)
		prevOutputName = prevConf.setMatches[setVarName]

	f = open('.'.join([prevOutputName,'restart','xsc']),'r')
	inputData = f.readlines()

	if len(inputData) > 2:
		lastLine = inputData[2].split()
		lastTimeStep = int(lastLine[0])
	else:
		print 'Your restart.xsc file is corrupted.'
	
	f.close()

	# Modify parameters of the namdConf object according to the parameters given.
	newConf = copy.deepcopy(prevConf)
	newConf.restart = True
	newConf.inputName = prevOutputName
	newConf.firsttimestep = lastTimeStep
	runTimeSteps = finalTimeStep - lastTimeStep

	if mode == 'min':
		newConf.mode = 'min'
		newConf.minimize = runTimeSteps
	elif mode == 'run':
		newConf.mode = 'run'
		newConf.run = runTimeSteps

	if outputName == False:
		outputName = ('restart_%s_%s' % (lastTimeStep,finalTimeStep))

	newConf.outputName = outputName

	for (key,value) in newConf.setMatches.iteritems():
		inputMatch = re.search(r'inputname',key,re.IGNORECASE)
		if inputMatch:
			newConf.setMatches[key] = newConf.inputName
		outputMatch = re.search(r'outputname',key,re.IGNORECASE)
		if outputMatch:
			newConf.setMatches[key] = newConf.outputName

	newConf.writeConf('.'.join([outputName,'conf']))

# CALLS

continueConf(args.prevconf[0],args.finalstep[0],outputName=args.outputname,mode=args.mode)