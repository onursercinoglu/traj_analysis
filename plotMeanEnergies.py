#!/usr/bin/env python

# IMPORTS
import argparse
import pickle
import matplotlib.pyplot as plt
import numpy as np
import pandas
from prody import *

# INPUT PARSING
parser = argparse.ArgumentParser(description='Plot a heatmap of mean energies \
	from a previously computed pairwise residue interaction energies pickle')

parser.add_argument('pdb',type=str,nargs=1,help='Name of the corresponding pdb \
 file of the energies pickle file')

parser.add_argument('pickle',type=str,nargs=1,help='Name of the energies pickle\
	 file')

parser.add_argument('--framerange',type=int,default=[False],nargs='+',
	help='If specified, then only FRAMERANGE section of the energies pickle \
	will be handled')

parser.add_argument('--sourcesel',type=str,nargs='+',
	help='A ProDy atom selection string which determines the first group of \
	selected residues. ')

parser.add_argument('--targetsel',type=str,nargs='+',
	help='A ProDy atom selection string which determines the second group of \
	selected residues.')

parser.add_argument('--vminmax',type=int,default=[50],nargs=1,
	help='Lower and upper limits of the heatmap')

parser.add_argument('--outfile',type=str,nargs=1,default=['meanEnergies.dat'],
	help='If specified, the resulting mean energies heatmap will be written into OUTFILE')

args = parser.parse_args()

# METHODS

def plotMeanEnergies(pdb,pickleFile,sourceSel='all',targetSel='all',
	frameRange=False,energyMinMax=[50],outFile='meanEnergies.dat'):
	
	print energyMinMax[0]
	energyMinMax = [(energyMinMax[0] - 2*energyMinMax[0]),energyMinMax[0]]
	
	energies = pickle.load(open(pickleFile,'r'))
	numFrames = len(energies['Total'][0][0].values)
	numResidues = len(energies['Total'])

	if frameRange.all() == False:
		frameRange = [0,numFrames]
	
	system = parsePDB(pdb)
	source = system.select(sourceSel)
	sourceResids = np.unique(source.getResindices())
	numSourceRes = len(sourceResids)

	target = system.select(targetSel)
	targetResids = np.unique(target.getResindices())
	numTargetRes = len(targetResids)
	
	heatMapEnergies = np.zeros([numSourceRes,numTargetRes])

	for i in range(0,numSourceRes):
		for j in range(0,numTargetRes):
			meanEnergies = np.mean(energies['Total'][sourceResids[i]][targetResids[j]].values[frameRange[0]:frameRange[1]])
			heatMapEnergies[i][j] = meanEnergies

	np.savetxt(outFile,heatMapEnergies)

	fig, ax = plt.subplots()
	heatmap = ax.pcolor(heatMapEnergies,cmap=plt.cm.Spectral, alpha=0.8, vmin=energyMinMax[0], vmax=energyMinMax[1])
	ax.set_frame_on(True)
	ax.grid(True)
	ax.set_yticks(map(int,np.linspace(0,numSourceRes,21)))
	ax.set_yticklabels(map(int,np.linspace(sourceResids[0],sourceResids[-1],21)))
	ax.set_ylabel('Residues')
	ax.set_xticks(map(int,np.linspace(0,numTargetRes,21)))
	ax.set_xticklabels(map(int,np.linspace(targetResids[0],targetResids[-1],21)))
	ax.set_xlabel('Residues')
	plt.colorbar(heatmap)
	fig = plt.gcf()
	plt.show()

# CALLS

if len(args.sourcesel) > 1:
	sourcesel = ' '.join(args.sourcesel)
else:
	sourcesel = args.sourcesel[0]

if len(args.targetsel) > 1:
	targetsel = ' '.join(args.targetsel)
else:
	targetsel = args.targetsel[0]

if len(args.framerange) > 1:
	framerange = np.asarray(args.framerange)
else:
	framerange = args.framerange[0]

vminmax = args.vminmax[0]

plotMeanEnergies(args.pdb[0],args.pickle[0],sourceSel=sourcesel,targetSel=targetsel,
	frameRange=framerange,energyMinMax=args.vminmax,outFile=args.outfile[0])